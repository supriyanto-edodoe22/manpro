<?php
namespace Proyek\models;
defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
use \MyLibraries\Libraries\IDnumber_generator as Autonumber;
use Illuminate\Database\Capsule\Manager as Capsule;

class Rab_model extends Eloquent
{
    protected $table = "rab";
    protected $fillable = ['*'];
    public $timestamps = false;
    protected $data = array();
    protected $appends = array();
    protected $return = array();
    protected $res = array('status' => false, 'message' => 'Error');

    //call_method Model
    public function call_method($method, $type = '')
    {
        $this->$method();

        return $this->res;
    }

    public function list_rab()
    {
        $columns = $_GET['columns'];
        $search = $_GET['search']['value'];
        $get_data = Eloquent::select("*")->where('rab.id_proyek','=',$_SESSION['id_proyek'])
            ->leftJoin('pekerjaan', 'pekerjaan.id_pekerjaan', '=', 'rab.id_pekerjaan');

        $val = $_GET['search']['value'];
        if (!empty($val))
            $get_data->where(function ($ds) use ($columns, $search) {
                foreach ($columns as $i => $v) {
                    if (!empty($v['data']) && $v['searchable'] == 'true')
                        $ds->orWhere($v['data'], 'LIKE', '%' . $search . '%');
                }
            });

        // order
        foreach ($_GET['order'] as $i => $o) {
            $get_data->orderBy($columns[$o['column']]['data'], $o['dir']);
        }

        $this->data = $get_data->take($_GET['length'])->offset($_GET['start'])->get();
        $i = 1;
        foreach ($this->data as $key => $value) {
            $value->no = $i;
            $value->aksi = "<i class=\"fa fa-pencil fa-fw text-primary\" style='cursor: pointer'></i>";
            $value->aksi .= "<i class=\"fa fa-trash fa-fw text-primary\" style='cursor: pointer'></i>";
            $value->detail = array(
                "material" => Capsule::table('rab_material')->where('id_rab','=',$value->id_rab)->leftJoin('material', 'material.id_material', '=', 'rab_material.id_material')->get(),
                "pekerja" => Capsule::table('rab_pekerja')->where('id_rab','=',$value->id_rab)->leftJoin('pekerja', 'pekerja.id_pekerja', '=', 'rab_pekerja.id_pekerja')->get(),
                "peralatan" => Capsule::table('rab_peralatan')->where('id_rab','=',$value->id_rab)->leftJoin('peralatan', 'peralatan.id_peralatan', '=', 'rab_peralatan.id_peralatan')->get(),
            );
            $i++;
        }


        $val = $_GET['search']['value'];
        if (!empty($val))
            $count = Eloquent::where('id_proyek','=',$_SESSION['id_proyek'])->where(function ($ds) use ($columns, $search) {
                foreach ($columns as $i => $v) {
                    if (!empty($v['data']) && $v['searchable'] == 'true')
                        $ds->orWhere($v['data'], 'LIKE', '%' . $search . '%');
                }
            });


        $this->res = array(
            'recordsTotal' => isset($count) ? $count->count() : Eloquent::count(),
            'recordsFiltered' => isset($count) ? $count->count() : Eloquent::count(),
            'data' => $this->data
        );

        return $this->res;
    }

    public function insert()
    {
        $status = false;
        Capsule::beginTransaction();

        $autonumber = new Autonumber();
        $_POST['header']['id_rab'] = $autonumber->number($autonumber->last_num($this->table,'id_rab','1'),'1');

        $Rab_model = new Rab_model();

        foreach ($_POST['header'] as $key_header => $val_header){
            $Rab_model->$key_header = $val_header;
        }
        
        $status = $Rab_model->save();

        foreach ($_POST['detail'] as $key_detail => $val_detail){
            switch ($val_detail['jenis']) {
                case 'rab_material':
                    $item_id = 'id_material';
                    break;
                case 'rab_peralatan':
                    $item_id = 'id_peralatan';
                    break;
                case 'rab_pekerja':
                    $item_id = 'id_pekerja';
                    break;
            }

            $status = Capsule::table($val_detail['jenis'])->insert(
                array(
                    $item_id => $val_detail['id_item_child'],
                    "satuan" => $val_detail['satuan'],
                    "kuantitas" => $val_detail['kuantiti'],
                    "id_rab" => $_POST['header']['id_rab']
            ));
        }


        if ($status) {
            Capsule::commit();
            $this->res = array('status' => true, 'message' => 'Success');
        } else {
            Capsule::rollBack();
        }
    }

    public function edit()
    {
 /*       echo "<pre>";
        print_r ($_POST);
        echo "</pre>";
        die();*/

        $status = false;
        Capsule::beginTransaction();
        
        $status = Eloquent::where('id_rab', '=', $_POST['header']['id_rab'])->update($_POST['header']);

        foreach ($_POST['detail'] as $key_detail => $val_detail){
            switch ($val_detail['jenis']) {
                case 'rab_material':
                    $item_id = 'id_material';
                    $id_detail = 'id_rab_material';
                    break;
                case 'rab_peralatan':
                    $item_id = 'id_peralatan';
                    $id_detail = 'id_rab_peralatan';
                    break;
                case 'rab_pekerja':
                    $item_id = 'id_pekerja';
                    $id_detail = 'id_rab_pekerja';
                    break;
            }

            $check = Capsule::table($val_detail['jenis'])->where($id_detail, array($val_detail['id_detail']))->get();

            if(count($check) == 0){
                $status = Capsule::table($val_detail['jenis'])->insert(
                    array(
                        $item_id => $val_detail['id_item_child'],
                        "satuan" => $val_detail['satuan'],
                        "kuantitas" => $val_detail['kuantiti'],
                        "id_rab" => $_POST['header']['id_rab']
                ));
            }else{
                $status = Capsule::table($val_detail['jenis'])->where($id_detail, '=', $val_detail['id_detail'])->update(
                    array(
                        $item_id => $val_detail['id_item_child'],
                        "satuan" => $val_detail['satuan'],
                        "kuantitas" => $val_detail['kuantiti'],
                        "id_rab" => $_POST['header']['id_rab']
                ));
            }

        }


        if ($status) {
            Capsule::commit();
            $this->res = array('status' => true, 'message' => 'Success');
        } else {
            Capsule::rollBack();
        }
            
    }

    public function delete()
    {
        Capsule::table('rab_material')->whereIn('id_rab', array($_POST['id']))->delete();
        Capsule::table('rab_pekerja')->whereIn('id_rab', array($_POST['id']))->delete();
        Capsule::table('rab_peralatan')->whereIn('id_rab', array($_POST['id']))->delete();

        Eloquent::where('id_rab', '=', $_POST['id'])->delete();

        $this->res = array('status' => true, 'message' => 'Success');
    }
}
