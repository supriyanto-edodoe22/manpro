<?php
namespace Proyek\models;
defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as Capsule;

class Input_progress_model extends Eloquent
{
    protected $table = "realisasi_pekerjaan";
    protected $fillable = ['*'];
    public $timestamps = false;
    protected $data = array();
    protected $appends = array();
    protected $return = array();
    protected $res = array('status' => false, 'message' => 'Error');

    //call_method Model
    public function call_method($method, $type = '')
    {
        $this->$method();

        return $this->res;
    }

    public function list_progress_harian()
    {
        $columns = $_GET['columns'];
        $search = $_GET['search']['value'];
        $get_data = Eloquent::select("id_realisasi","pekerjaan.id_pekerjaan","nama","bobot","volume",Capsule::raw('SUM(volume_realisasi) as volume_realisasi'))->from('pekerjaan')
            ->leftJoin('realisasi_pekerjaan', 'realisasi_pekerjaan.id_pekerjaan', '=', 'pekerjaan.id_pekerjaan');

        $val = $_GET['search']['value'];
        if (!empty($val))
            $get_data->where('realisasi_pekerjaan.tipe','=',1)->where(function ($ds) use ($columns, $search) {
                foreach ($columns as $i => $v) {
                    if (!empty($v['data']) && $v['searchable'] == 'true')
                        $ds->orWhere($v['data'], 'LIKE', '%' . $search . '%');
                }
            });

        // order
        foreach ($_GET['order'] as $i => $o) {
            $get_data->orderBy($columns[$o['column']]['data'], $o['dir']);
        }

        $get_data->groupBy('nama');

        $this->data = $get_data->take($_GET['length'])->offset($_GET['start'])->get();
        $i = 1;
        foreach ($this->data as $key => $value) {
            $value->no = $i;
            $value->bobot_realisasi = ($value->volume_realisasi/$value->volume*100)/100*$value->bobot;
            $value->volume_realisasi = ($value->volume_realisasi != '' ) ? $value->volume_realisasi : 0;
//            $value->aksi = "<i class=\"fa fa-pencil fa-fw text-primary\" style='cursor: pointer'></i>";
//            $value->aksi .= "<i class=\"fa fa-trash fa-fw text-primary\" style='cursor: pointer'></i>";
            $i++;
        }


        $val = $_GET['search']['value'];
        if (!empty($val))
            $count = Eloquent::from('pekerjaan')
                ->leftJoin('realisasi_pekerjaan', 'realisasi_pekerjaan.id_pekerjaan', '=', 'pekerjaan.id_pekerjaan')
                ->where('realisasi_pekerjaan.tipe','=',1)
                ->where(function ($ds) use ($columns, $search) {
                foreach ($columns as $i => $v) {
                    if (!empty($v['data']) && $v['searchable'] == 'true')
                        $ds->orWhere($v['data'], 'LIKE', '%' . $search . '%');
                }
            })->groupBy('nama');


        $this->res = array(
            'recordsTotal' => isset($count) ? $count->count() : Eloquent::count(),
            'recordsFiltered' => isset($count) ? $count->count() : Eloquent::count(),
            'data' => $this->data
        );

        return $this->res;
    }

    public function insert()
    {
        $input_progress_model = new input_progress_model();
        //hapus kiriman primary table dari POST

        $split = explode("_",$_POST['tanggal']);
        $_POST['periode'] = $split[0];
        $_POST['tanggal'] = $split[1];

        $total_harga_pekerjaan = \Proyek\models\Rab_model::where('id_pekerjaan','=',$_POST['id_pekerjaan'])
                            ->first(['total']);

        $_POST['bobot_realisasi'] =  ($_POST['actual_cost'] / (isset($total_harga_pekerjaan->total) ? $total_harga_pekerjaan->total : 1) ) * (100/100);

        unset($_POST['id_realisasi']);
        unset($_POST['tipe']);

        foreach ($_POST as $key => $val){
            $input_progress_model->$key = $val;
        }

        if ($input_progress_model->save()) {
            $this->res = array('status' => true, 'message' => 'Success');
        }else
            $this->res = array('status' => false, 'message' => 'Error');
    }
}
