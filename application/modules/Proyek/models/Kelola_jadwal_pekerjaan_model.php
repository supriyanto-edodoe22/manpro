<?php
namespace Proyek\models;
defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
use \MyLibraries\Libraries\IDnumber_generator as Autonumber;
class Kelola_jadwal_pekerjaan_model extends Eloquent
{
    protected $table = "pekerjaan";
    protected $fillable = ['*'];
    public $timestamps = false;
    protected $data = array();
    protected $appends = array();
    protected $return = array();
    protected $res = array('status' => false, 'message' => 'Error');

    //call_method Model
    public function call_method($method, $type = '')
    {
        $this->$method();

        return $this->res;
    }

    public function insert()
    {
        $autonumber = new Autonumber();
        $Kelola_data_proyek_model = new Kelola_data_proyek_model();
        $Kelola_data_proyek_model->id_proyek = $autonumber->number($autonumber->last_num($this->table,'id_proyek','1'),'1');

        unset($_POST['id_proyek']);
        foreach ($_POST as $key => $val){
            $Kelola_data_proyek_model->$key = $val;
        }

        if ($Kelola_data_proyek_model->save()) {
            $this->res = array('status' => true, 'message' => 'Success');
        }
    }

    public function edit()
    {
        Eloquent::where('id_proyek', '=', $_POST['id_proyek'])->update($_POST);
        $this->res = array('status' => true, 'message' => 'Success');
    }

    public function update_status()
    {
        $_POST['edit_date'] = date('Y-m-d');
        $_POST['edit_by'] = $_SESSION['user_id'];
        Eloquent::where('id', '=', $_POST['id'])->update(array('status' => $_POST['status']));
        $this->res = array('status' => true, 'message' => 'Success');
    }

    public function cpm(){
        $this->res = array('data' => Eloquent::get() );
    }
}
