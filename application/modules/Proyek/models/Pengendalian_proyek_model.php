<?php
namespace Proyek\models;
defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as Capsule;

class Pengendalian_proyek_model extends Eloquent
{
    protected $table = "realisasi_pekerjaan";
    protected $fillable = ['*'];
    public $timestamps = false;
    protected $data = array();
    protected $appends = array();
    protected $return = array();
    protected $res = array('status' => false, 'message' => 'Error');

    //call_method Model
    public function call_method($method, $type = '')
    {
        $this->$method();

        return $this->res;
    }

    public function generate_evm()
    {
        Capsule::raw('SET sql_mode = \'\';');
        $get_data = Eloquent::select("id_realisasi","pekerjaan.id_pekerjaan","nama","bobot","volume","periode",Capsule::raw('SUM(volume_realisasi) as volume_realisasi'))
            ->from('realisasi_pekerjaan')
            ->leftJoin('pekerjaan', 'pekerjaan.id_pekerjaan', '=', 'realisasi_pekerjaan.id_pekerjaan');

        $get_data->groupBy('periode');

        $this->data = $get_data->get();
/*        $i = 1;
        foreach ($this->data as $key => $value) {
            $value->no = $i;
            $value->bobot_realisasi = ($value->volume_realisasi/$value->volume*100)/100*$value->bobot;
            $value->volume_realisasi = ($value->volume_realisasi != '' ) ? $value->volume_realisasi : 0;
            $i++;
        }*/

        $val = $_GET['search']['value'];

        $this->res = array(
            'data' => $this->data
        );

        return $this->res;
    }

    public function insert()
    {
        $input_progress_model = new input_progress_model();
        //hapus kiriman primary table dari POST

        $split = explode("_",$_POST['tanggal']);
        $_POST['periode'] = $split[0];
        $_POST['tanggal'] = $split[1];

        $total_harga_pekerjaan = \Proyek\models\Rab_model::where('id_pekerjaan','=',$_POST['id_pekerjaan'])
                            ->first(['total']);

        $_POST['bobot_realisasi'] =  ($_POST['actual_cost'] / $total_harga_pekerjaan->total) * (100/100);

        unset($_POST['id_realisasi']);
        unset($_POST['tipe']);

        foreach ($_POST as $key => $val){
            $input_progress_model->$key = $val;
        }

        if ($input_progress_model->save()) {
            $this->res = array('status' => true, 'message' => 'Success');
        }else
            $this->res = array('status' => false, 'message' => 'Error');
    }
}
