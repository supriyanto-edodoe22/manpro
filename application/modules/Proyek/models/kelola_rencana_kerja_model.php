<?php
namespace Proyek\models;
defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
use MyLibraries\Libraries\IDnumber_generator as Autonumber;

class kelola_rencana_kerja_model extends Eloquent
{
    protected $table = "rencana_pekerjaan";
    protected $fillable = ['*'];
    public $timestamps = false;
    protected $data = array();
    protected $appends = array();
    protected $return = array();
    protected $res = array('status' => false, 'message' => 'Error');

    //call_method Model
    public function call_method($method, $type = '')
    {
        $this->$method();

        return $this->res;
    }

    public function insert()
    {
        $kelola_rencana_kerja_model = new kelola_rencana_kerja_model();

        if($_POST['tipe'] == 2 ){
            $split = explode("_",$_POST['tanggal']);
            $_POST['dari'] = $split[0];
            $_POST['sampai'] = $split[1];
        }else{
            $_POST['dari'] = $_POST['tanggal'];
            $_POST['sampai'] = $_POST['tanggal'];
        }
        unset($_POST['id_realisasi']);
        unset($_POST['tanggal']);

        foreach ($_POST as $key => $val){
            $kelola_rencana_kerja_model->$key = $val;
        }

        if ($kelola_rencana_kerja_model->save()) {
            $this->res = array('status' => true, 'message' => 'Success');
        }
    }
}
