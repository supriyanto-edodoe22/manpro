<?php
namespace Proyek\models;
defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
use \MyLibraries\Libraries\IDnumber_generator as Autonumber;
use Illuminate\Database\Capsule\Manager as Capsule;

class Kelola_data_proyek_model extends Eloquent
{
    protected $table = "proyek";
    protected $fillable = ['*'];
    public $timestamps = false;
    protected $data = array();
    protected $appends = array();
    protected $return = array();
    protected $res = array('status' => false, 'message' => 'Error');

    //call_method Model
    public function call_method($method, $type = '')
    {
        $this->$method();

        return $this->res;
    }

    public function list_proyek()
    {
        $columns = $_GET['columns'];
        $search = $_GET['search']['value'];
        $get_data = Eloquent::select("*");
        $val = $_GET['search']['value'];
        if (!empty($val))
            $get_data->where(function ($ds) use ($columns, $search) {
                foreach ($columns as $i => $v) {
                    if (!empty($v['data']) && $v['searchable'] == 'true')
                        $ds->orWhere($v['data'], 'LIKE', '%' . $search . '%');
                }
            });

        // order
        foreach ($_GET['order'] as $i => $o) {
            $get_data->orderBy($columns[$o['column']]['data'], $o['dir']);
        }

        $this->data = $get_data->take($_GET['length'])->offset($_GET['start'])->get();
        foreach ($this->data as $key => $value) {
            $struktur_kerja = Kelola_struktur_kerja_model::select(Capsule::raw('sum(volume) as total_volume'))
                ->where('id_proyek', '=', $value->id_proyek)
                ->first();
            $realisasi_kerja = Input_progress_model::select(Capsule::raw('sum(volume_realisasi) as total_volume'))
                ->join('pekerjaan', 'pekerjaan.id_pekerjaan', '=', 'realisasi_pekerjaan.id_pekerjaan')
                ->where('pekerjaan.id_proyek', '=', $value->id_proyek)
                ->first();

            $progress = ($realisasi_kerja->total_volume / $struktur_kerja->total_volume) * (100 / 100);

            $value->info = "
                <small>
                <i class=\"fa fa-map-marker\"></i>  &nbsp;Lokasi " . $value->lokasi . "<br>
                <i class=\"fa fa-user\"></i>  &nbsp;Pemilik " . $value->pemilik . "<br>
                <i class=\"fa fa-calendar\"></i>  &nbsp;Mulai " . date_format(date_create($value->tgl_mulai), "d/m/Y") . "<br>
                <i class=\"fa fa-calendar\"></i> &nbsp;Selesai " . date_format(date_create($value->tgl_selesai), "d/m/Y") . "<br> 
                <i class=\"fa fa-money\"></i> Nilai Rp. " . $value->nilai . "<br>
                <i class=\"fa fa-check-circle-o\"></i>  &nbsp;Status <span class=\"text-default\" type=\"button\">PLANNING</span><br>
            ";
            //<span class="text-danger"><i class="fa fa-clock-o"></i>  Terlambat <strong>24</strong> hari kalender</span></small>
            $value->progress = "
                <small><strong><span class=\"text-danger\">" . number_format($progress, 2) . "%</span></strong> Complete</small>
                <div class=\"progress progress-sm\"><div aria-valuenow=\"0\" style=\"width: " . number_format($progress, 2) . "%;\" class=\"progress-bar progress-bar-danger done\" role=\"progressbar\" data-transitiongoal=\"0\"></div></div>
            ";
//            $value->aksi = "<a data-toggle=\"modal\" href='#edit-proyek'><i class=\"fa fa-eye fa-fw text-primary select_data\"></i></a>";
//            $value->aksi .= "<a data-toggle=\"modal\" href='#edit-proyek'><i class=\"fa fa-pencil fa-fw text-primary select_data\"></i></a>";
//            $value->aksi .= "<a data-toggle=\"modal\" href='#delete-proyek'><i class=\"fa fa-trash fa-fw text-primary\"></i></a>";
            $value->aksi = "
                <div class=\"dropdown\">
                  <button class=\"btn btn-default dropdown-toggle\" type=\"button\" id=\"dropdownMenu1\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">
                    Kelola Project
                    <span class=\"caret\"></span>
                  </button>
                  <ul class=\"dropdown-menu\" aria-labelledby=\"dropdownMenu1\">
                    <li><a class=\"ubah\" data-toggle=\"modal\" href='javascript:void(0)'><i class=\"fa fa-pencil-square-o fa-fw select_data\">&nbsp;Ubah</i></a></li>
                    <li><a class='redirect_page' page='Proyek/KelolaRencanaKerja/' href=\"javascript:void(0)\"><i class=\"fa fa-tasks fa-fw select_data\">&nbsp;Kelola Rencana Kerja</i></a></li>
                    <li><a class='redirect_page' page='Proyek/KelolaJadwalPekerjaan/' href=\"javascript:void(0)\"><i class=\"fa fa-calendar-o fa-fw select_data\">&nbsp;Kelola Jadwal Pekerjaan</i></a></li>
                    <li><a class='redirect_page' page='Proyek/KelolaStrukturKerja/' href=\"javascript:void(0)\"><i class=\"fa fa-sitemap fa-fw select_data\">&nbsp;Kelola Struktur Kerja</i></a></li>
                    <li><a class='redirect_page' page='Proyek/ManajemenResiko/' href=\"javascript:void(0)\"><i class=\"fa fa-chain-broken fa-fw select_data\">&nbsp;Manajemen Resiko</i></a></li>
                    <li><a class='redirect_page' page='Proyek/PengendalianProyek' href=\"javascript:void(0)\"><i class=\"fa fa-cog fa-fw select_data\">&nbsp;Pengendalian Proyek</i></a></li>
                    <li><a class='redirect_page' page='Proyek/KelolaRAB/' href=\"javascript:void(0)\"><i class=\"fa fa-money fa-fw select_data\">&nbsp;Kelola RAB</i></a></li>
                    <li><a class='redirect_page' page='Proyek/InputProgress/harian/' href=\"javascript:void(0)\"><i class=\"fa fa-file-text-o fa-fw select_data\">&nbsp;Input Progress Harian</i></a></li>
                  </ul>
                </div>
            ";
            //<li><a class='redirect_page' page='Proyek/InputProgress/mingguan/' href="javascript:void(0)"><i class="fa fa-file-text-o fa-fw select_data">&nbsp;Input Progress Mingguan</i></a></li>
        }


        $val = $_GET['search']['value'];
        if (!empty($val))
            $count = Eloquent::where(function ($ds) use ($columns, $search) {
                foreach ($columns as $i => $v) {
                    if (!empty($v['data']) && $v['searchable'] == 'true')
                        $ds->orWhere($v['data'], 'LIKE', '%' . $search . '%');
                }
            });


        $this->res = array(
            'recordsTotal' => isset($count) ? $count->count() : Eloquent::count(),
            'recordsFiltered' => isset($count) ? $count->count() : Eloquent::count(),
            'data' => $this->data
        );

        return $this->res;
    }

    public function insert()
    {
        $autonumber = new Autonumber();
        $Kelola_data_proyek_model = new Kelola_data_proyek_model();
        $Kelola_data_proyek_model->id_proyek = $autonumber->number($autonumber->last_num($this->table, 'id_proyek', '1'), '1');

        unset($_POST['id_proyek']);
        foreach ($_POST as $key => $val) {
            $Kelola_data_proyek_model->$key = $val;
        }

        if ($Kelola_data_proyek_model->save()) {
            $this->res = array('status' => true, 'message' => 'Success');
        }
    }

    public function edit()
    {
        Eloquent::where('id_proyek', '=', $_POST['id_proyek'])->update($_POST);
        $this->res = array('status' => true, 'message' => 'Success');
    }

    public function update_status()
    {
        $_POST['edit_date'] = date('Y-m-d');
        $_POST['edit_by'] = $_SESSION['user_id'];
        Eloquent::where('id', '=', $_POST['id'])->update(array('status' => $_POST['status']));
        $this->res = array('status' => true, 'message' => 'Success');
    }
}
