<?php
namespace Proyek\models;
defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;

use MyLibraries\Libraries\IDnumber_generator as Autonumber;

class Kelola_struktur_kerja_model extends Eloquent
{
    protected $table = "pekerjaan";
    protected $fillable = ['*'];
    public $timestamps = false;
    protected $data = array();
    protected $appends = array();
    protected $return = array();
    protected $res = array('status' => false, 'message' => 'Error');

    //call_method Model
    public function call_method($method, $type = '')
    {
        $this->$method();
        return $this->res;
    }

    public function list_struktur_kerja()
    {
        $columns = $_GET['columns'];
        $search = $_GET['search']['value'];
        $get_data = Eloquent::select("*")->where('id_proyek','=',$_SESSION['id_proyek']);

        $val = $_GET['search']['value'];
        if (!empty($val))
            $get_data->where(function ($ds) use ($columns, $search) {
                foreach ($columns as $i => $v) {
                    if (!empty($v['data']) && $v['searchable'] == 'true')
                        $ds->orWhere($v['data'], 'LIKE', '%' . $search . '%');
                }
            });

        // order
        foreach ($_GET['order'] as $i => $o) {
            $get_data->orderBy($columns[$o['column']]['data'], $o['dir']);
        }

        $this->data = $get_data->take($_GET['length'])->offset($_GET['start'])->get();
        $i = 1;
        foreach ($this->data as $key => $value) {
            $value->no = $i;
            $value->aksi = "<i class=\"fa fa-pencil fa-fw text-primary\" style='cursor: pointer'></i>";
            $value->aksi .= "<i class=\"fa fa-trash fa-fw text-primary\" style='cursor: pointer'></i>";
            $i++;
        }


        $val = $_GET['search']['value'];
        if (!empty($val))
            $count = Eloquent::where('id_proyek','=',$_SESSION['id_proyek'])->where(function ($ds) use ($columns, $search) {
                foreach ($columns as $i => $v) {
                    if (!empty($v['data']) && $v['searchable'] == 'true')
                        $ds->orWhere($v['data'], 'LIKE', '%' . $search . '%');
                }
            });


        $this->res = array(
            'recordsTotal' => isset($count) ? $count->count() : Eloquent::where('id_proyek','=',$_SESSION['id_proyek'])->count(),
            'recordsFiltered' => isset($count) ? $count->count() : Eloquent::where('id_proyek','=',$_SESSION['id_proyek'])->count(),
            'data' => $this->data
        );

        return $this->res;
    }

    public function insert()
    {
        $kelola_struktur_kerja_model = new kelola_struktur_kerja_model();
        $autonumber = new Autonumber();
        $kelola_struktur_kerja_model->id_pekerjaan = $autonumber->number($autonumber->last_num($this->table,'id_pekerjaan','1'),'1');

        //hapus kiriman primary table dari POST
        unset($_POST['id_pekerjaan']);
        $_POST['pendahulu'] = (empty($_POST['pendahulu'])) ? NULL : json_encode($_POST['pendahulu']);
        foreach ($_POST as $key => $val){
            $kelola_struktur_kerja_model->$key = $val;
        }

        if ($kelola_struktur_kerja_model->save()) {
            $this->res = array('status' => true, 'message' => 'Success');
        }
    }

    public function edit()
    {
        $_POST['pendahulu'] = (empty($_POST['pendahulu'])) ? NULL : json_encode($_POST['pendahulu']);
        if(Eloquent::where('id_pekerjaan', '=', $_POST['id_pekerjaan'])->update($_POST))
            $this->res = array('status' => true, 'message' => 'Success');
    }

    public function delete()
    {
        if(Eloquent::where('id_pekerjaan', '=', $_POST['id'])->delete())
            $this->res = array('status' => true, 'message' => 'Success');
    }
}
