<?php defined('BASEPATH') OR exit('No direct script access allowed');

class KelolaStrukturKerja extends Private_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->lang->load('auth');
    }

    public function index()
    {
        $_SESSION['id_proyek'] = $this->input->post('id_proyek');
        $model = new \Proyek\models\kelola_struktur_kerja_model();

        $js = array(
            'modules/Proyek/KelolaStrukturKerja'
        );

        $data['data_pekerjaan'] = $model->where('id_proyek','=',$_SESSION['id_proyek'])->get();
        $data['id_proyek'] = $_SESSION['id_proyek'];
        $data['nama_proyek'] = $this->input->post('nama_proyek');

        $this->render("Proyek", "kelola_struktur_kerja_view", $data, true, $js, array());
    }

    public function grid($grid)
    {
        $model = new \Proyek\models\kelola_struktur_kerja_model();
        echo json_encode($model->call_method($grid, 'grid'));
    }

    public function ajax($action)
    {
        $model = new \Proyek\models\kelola_struktur_kerja_model();
        echo json_encode($model->call_method($action, 'action'));
    }
}
