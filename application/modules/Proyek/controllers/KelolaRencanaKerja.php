<?php defined('BASEPATH') OR exit('No direct script access allowed');

class KelolaRencanaKerja extends Private_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->lang->load('auth');
    }

    public function index()
    {
        $_SESSION['id_proyek'] = $this->input->post('id_proyek');
        $data['id_proyek'] = $_SESSION['id_proyek'];
        $data['nama_proyek'] = $this->input->post('nama_proyek');

        $model = new \Proyek\models\Kelola_struktur_kerja_model();
        $data['data_pekerjaan'] = $model->where('id_proyek','=',$_SESSION['id_proyek'])->get();
        $proyek = new \Proyek\models\Kelola_data_proyek_model();
        $get_data_proyek = $proyek->where('id_proyek','=',$_SESSION['id_proyek'])->first();
        $data['jumlah_minggu'] = jumlah_minggu($get_data_proyek->tgl_mulai,$get_data_proyek->tgl_selesai);
        $data['tanggal_minggu'] = tanggal_minggu($get_data_proyek->tgl_mulai,$get_data_proyek->tgl_selesai);

        $mulai = strtotime($get_data_proyek->tgl_mulai);
        $selesai = strtotime($get_data_proyek->tgl_selesai);
        $jumlah_detik = abs($selesai - $mulai);
        $jumlah_hari = $jumlah_detik/86400;
        $jumlah_hari = intval($jumlah_hari);
        $data['jumlah_hari']=$jumlah_hari;

        $js = array(
            'modules/Proyek/KelolaRencanaKerja',
            'select2.min'
        );

        $this->render("Proyek", "kelola_rencana_kerja_view", $data, true, $js, array('select2.min'));
    }

    public function grid($grid)
    {
        $model = new \Proyek\models\Kelola_rencana_kerja_model();
        echo json_encode($model->call_method($grid, 'grid'));
    }

    public function ajax($action)
    {
        $model = new \Proyek\models\Kelola_rencana_kerja_model();
        echo json_encode($model->call_method($action, 'action'));
    }
}
