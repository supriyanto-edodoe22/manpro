<?php defined('BASEPATH') OR exit('No direct script access allowed');

class KelolaDataProyek extends Private_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $mandor = new \Konfigurasi\models\Pengguna_model();
        $data['mandor'] = $mandor->list_mandor();
        $this->render("Proyek", "kelola_data_proyek_view", $data, true, array('modules/Proyek/KelolaDataProyek'));

    }

    public function grid($grid)
    {
        $model = new \Proyek\models\Kelola_data_proyek_model();
        echo json_encode($model->call_method($grid, 'grid'));
    }

    public function ajax($action)
    {
        $model = new \Proyek\models\Kelola_data_proyek_model();
        echo json_encode($model->call_method($action, 'action'));
    }
}
