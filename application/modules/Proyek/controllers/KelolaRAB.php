<?php defined('BASEPATH') OR exit('No direct script access allowed');

class KelolaRAB extends Private_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->lang->load('auth');
    }

    public function index()
    {
        $_SESSION['id_proyek'] = $this->input->post('id_proyek');
        $data['id_proyek'] = $_SESSION['id_proyek'];
        $data['nama_proyek'] = $this->input->post('nama_proyek');

        $model_pekerjaan = new \Proyek\models\kelola_struktur_kerja_model();
        $data['data_pekerjaan'] = $model_pekerjaan->where('id_proyek','=',$data['id_proyek'])->get(['id_pekerjaan as id','nama as name']);

/*        foreach ($model_pekerjaan->where('id_proyek','=',$data['id_proyek'])->get() as $key => $value) {
            $data['data_pekerjaan'][] = array("id" => $value->id_pekerjaan , "name"=> $value->nama );
        }*/

        $model_material = new \Konfigurasi\models\Kelola_harga_material_model();
        $data['material'] = $model_material->get(['id_material as id','nama as name','harga']);

        $model_pekerja = new \Konfigurasi\models\Kelola_harga_pekerja_model();
        $data['pekerja'] = $model_pekerja->get(['id_pekerja as id','jenis as name','harga']);

        $model_peralatan = new \Konfigurasi\models\Kelola_harga_peralatan_model();
        $data['peralatan'] = $model_peralatan->get(['id_peralatan as id','nama as name','harga']);

        $this->render("Proyek", "kelola_RAB_view", $data, true, array('modules/Proyek/KelolaRAB'));
    }

    public function grid($grid)
    {
        $model = new \Proyek\models\Rab_model();
        echo json_encode($model->call_method($grid, 'grid'));
    }

    public function ajax($action)
    {
        $model = new \Proyek\models\Rab_model();
        echo json_encode($model->call_method($action, 'action'));
    }
}
