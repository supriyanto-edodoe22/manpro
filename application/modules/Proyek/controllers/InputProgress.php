<?php defined('BASEPATH') OR exit('No direct script access allowed');

class InputProgress extends Private_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->lang->load('auth');
        $js = array(
            'modules/Proyek/KelolaStrukturKerja',
            'select2.min'
        );
    }

    public function index()
    {
        $this->render("Proyek", "input_progress_view", $this->data, true, array('modules/Proyek/InputProgress','select2.min'),array('select2.min'));
    }

    public function harian(){
        $_SESSION['id_proyek'] = $this->input->post('id_proyek');
        $data['id_proyek'] = $_SESSION['id_proyek'];
        $data['nama_proyek'] = $this->input->post('nama_proyek');

        $model = new \Proyek\models\Kelola_struktur_kerja_model();
        $data['data_pekerjaan'] = $model->where('id_proyek','=',$_SESSION['id_proyek'])->get();
        $proyek = new \Proyek\models\Kelola_data_proyek_model();
        $get_data_proyek = $proyek->where('id_proyek','=',$_SESSION['id_proyek'])->first();

        $mulai = strtotime($get_data_proyek->tgl_mulai);
        $selesai = strtotime($get_data_proyek->tgl_selesai);
        $jumlah_detik = abs($selesai - $mulai);
        $jumlah_hari = $jumlah_detik/86400;
        $jumlah_hari = intval($jumlah_hari);
        $data['jumlah_hari']=$jumlah_hari;
        $data['periode_harian']=periode_harian($get_data_proyek->tgl_mulai,$get_data_proyek->tgl_selesai);

        $this->render("Proyek", "input_progress_harian_view", $data, true, array('modules/Proyek/InputProgressHarian','select2.min'),array('select2.min'));
    }

    public function mingguan(){
        $_SESSION['id_proyek'] = $this->input->post('id_proyek');
        $data['id_proyek'] = $_SESSION['id_proyek'];
        $data['nama_proyek'] = $this->input->post('nama_proyek');

        $model = new \Proyek\models\Kelola_struktur_kerja_model();
        $data['data_pekerjaan'] = $model->where('id_proyek','=',$_SESSION['id_proyek'])->get();
        $proyek = new \Proyek\models\Kelola_data_proyek_model();
        $get_data_proyek = $proyek->where('id_proyek','=',$_SESSION['id_proyek'])->first();
        $data['jumlah_minggu'] = jumlah_minggu($get_data_proyek->tgl_mulai,$get_data_proyek->tgl_selesai);
        $data['tanggal_minggu'] = tanggal_minggu($get_data_proyek->tgl_mulai,$get_data_proyek->tgl_selesai);

        $this->render("Proyek", "input_progress_mingguan_view", $data, true, array('modules/Proyek/InputProgressMingguan','select2.min'),array('select2.min'));
    }

    public function grid($grid)
    {
        $model = new \Proyek\models\Input_progress_model();
        echo json_encode($model->call_method($grid, 'grid'));
    }

    public function ajax($action)
    {
        $model = new \Proyek\models\Input_progress_model();
        echo json_encode($model->call_method($action, 'action'));
    }
}
