<?php defined('BASEPATH') OR exit('No direct script access allowed');
use Proyek\models\Kelola_jadwal_pekerjaan_model as PekerjaanModel;
use Proyek\models\Rab_model as RabModel;
use Illuminate\Database\Capsule\Manager as Capsule;

class PengendalianProyek extends Private_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->lang->load('auth');
    }

    public function index()
    {
        $_SESSION['id_proyek'] = $this->input->post('id_proyek');
        $data['id_proyek'] = $_SESSION['id_proyek'];
        $data['nama_proyek'] = $this->input->post('nama_proyek');



//        echo "<pre>";
//        print_r ($bac[0]->bac);
//        echo "</pre>";
//        die();

        $data['last_legiatan'] = PekerjaanModel::where('id_proyek','=',$_SESSION['id_proyek'])->orderBy('mulai','DESC')->first(['id_pekerjaan']);
        $data['pekerjaan'] = PekerjaanModel::where('id_proyek','=',$_SESSION['id_proyek'])->get(['id_pekerjaan','nama','mulai','durasi']);
        $data['last_legiatan'] = PekerjaanModel::where('id_proyek','=',$_SESSION['id_proyek'])->orderBy('mulai','DESC')->first(['id_pekerjaan']);
        $data['pekerjaan_gantt'] = PekerjaanModel::where('id_proyek','=',$_SESSION['id_proyek'])->get(['id_pekerjaan as id','nama as text',Capsule::raw('DATE_FORMAT(mulai,\'%d-%m-%Y\') AS start_date'),'durasi as duration']);

        $data['link'] = array();
        $data['link_gantt'] = array();

        $id_link = 1;

        foreach (PekerjaanModel::where('id_proyek','=',$_SESSION['id_proyek'])->get() as $key => $value) {
            $pendahulu = json_decode($value->pendahulu);
            if($pendahulu){                
                foreach ($pendahulu as $key_pendahulu => $value_pendahulu) {
                    $data['link'][] = array("from" => $value_pendahulu, "to" => $value->id_pekerjaan);
                    $data['link_gantt'][] = array("id" => $id_link+$key_pendahulu, "source" => $value_pendahulu, "target" => $value->id_pekerjaan, "type" => 0);
                }
            }

            $id_link++;
        }

        $proyek = new \Proyek\models\Kelola_data_proyek_model();
        $get_data_proyek = $proyek->where('id_proyek','=',$_SESSION['id_proyek'])->first();

        $mulai = strtotime($get_data_proyek->tgl_mulai);
        $selesai = strtotime($get_data_proyek->tgl_selesai);
        $jumlah_detik = abs($selesai - $mulai);
        $jumlah_hari = $jumlah_detik/86400;
        $jumlah_hari = intval($jumlah_hari);
        $data['jumlah_hari']=$jumlah_hari;

        $this->render("Proyek", "pengendalian_proyek_view", $data, true, array('modules/Proyek/PengendalianProyek'),array());
    }

    public function grid($grid)
    {
        $model = new \Proyek\models\Pengendalian_proyek_model();
        echo json_encode($model->call_method($grid, 'grid'));
    }

    public function ajax($action)
    {
//        $model = new \Proyek\models\Pengendalian_proyek_model();
//        echo json_encode($model->call_method($action, 'action'));

        $proyek = new \Proyek\models\Kelola_data_proyek_model();
        $get_data_proyek = $proyek->where('id_proyek','=',$_SESSION['id_proyek'])->first();

        $mulai = strtotime($get_data_proyek->tgl_mulai);
        $selesai = strtotime($get_data_proyek->tgl_selesai);
        $jumlah_detik = abs($selesai - $mulai);
        $jumlah_hari = $jumlah_detik/86400;
        $jumlah_hari = intval($jumlah_hari);

        $model = new \Proyek\models\Kelola_struktur_kerja_model();
        $data_pekerjaan = $model->where('id_proyek','=',$_SESSION['id_proyek'])->get();

        foreach ($data_pekerjaan as $key => $val) {
            $bobot_sebelumnya = 0;
            for ($i = 1; $i <= $jumlah_hari; $i++) {
                $bobot = (($i==1) ? ($val->bobot + $bobot_sebelumnya) / $val->durasi : ($i <= $val->durasi) ? ($val->bobot / $val->durasi) + $bobot_sebelumnya : $val->bobot ) ;
                $jumlah_bobot_harian_rencana[$i] = (isset($jumlah_bobot_harian[$i])) ? $jumlah_bobot_harian[$i] + $bobot  : $bobot;
                $bobot_sebelumnya = number_format($bobot,2);
            }
        }

        $jumlah_bobot = $jum_bot = 0;
        $jumlah_bobot_harian = $jumlah_volume_harian = $actual_cost_harian = array();
        $no = 1;
        foreach ($data_pekerjaan as $key => $val){
            $bobot_sebelumnya = 0;
            $kondisi_terpenuhi = false;
            for ($i=1;$i<=$jumlah_hari;$i++){
                $dt = $this->db
                    ->select_sum('bobot_realisasi')
                    ->select_sum('volume_realisasi')
                    ->select_sum('actual_cost')
                    ->where(array(
                        'id_pekerjaan' => $val->id_pekerjaan,
                        'periode' => $i))
                    ->group_by('periode')
                    ->get('realisasi_pekerjaan');
                $res = $dt->result();

                $volume_realisasi = (count($res) != 0) ? $res[0]->volume_realisasi : 0 ;
                $realisasi_bobot = ($volume_realisasi/$val->volume) * 100;
                $hasil_bobot = ($realisasi_bobot / 100) * $val->bobot;
                $bobot = ($i == 1) ? $hasil_bobot : ($hasil_bobot > 0) ? $hasil_bobot + $bobot_sebelumnya : 0;
                $kondisi_terpenuhi = ($bobot < $val->bobot) ? false : true;
                $validasi_bobot = ($kondisi_terpenuhi) ? $val->bobot : $bobot;
                $bobot_sebelumnya = $bobot;
                $jumlah_bobot_harian_aktual[$i] = (isset($jumlah_bobot_harian[$i])) ? $jumlah_bobot_harian[$i] + ((count($res) != 0) ? $bobot : 0) : ((count($res) != 0) ? $bobot : 0);
                $actual_cost_harian_aktual[$i] = (isset($actual_cost_harian[$i])) ? $actual_cost_harian[$i] + ((count($res) != 0) ? $res[0]->actual_cost : 0) : ((count($res) != 0) ? $res[0]->actual_cost : 0);
            }
        }

        $response = array();
        $get_bac = RabModel::where('id_proyek','=',$_SESSION['id_proyek'])->get([Capsule::raw('SUM(total) AS bac')]);
        for($i=1;$i<=$jumlah_hari;$i++){
            $BAC = $get_bac[0]->bac;
            $EV = $jumlah_bobot_harian_aktual[$i] * $BAC;
            $PV = $jumlah_bobot_harian_rencana[$i] * $BAC;
            $AC = $actual_cost_harian_aktual[$i];
            $OD = $_GET['od'];
            $SPI = $EV/$PV;
            $CPI = $EV/($AC > 0 ? $AC : 1 );
            $response[] = array(
                "periode"=>$i,
                "SV"=>$EV-$PV,
                "CV"=>$EV-$AC,
                "SPI"=>$SPI,
                "CPI"=>$CPI,
                "EAC"=>$BAC/($CPI > 0 ? $CPI : 1 ),
                "ETC"=>$OD/($SPI > 0 ? $SPI : 1 )
            );
        }

        echo json_encode(array('response'=>$response,'jumlah_hari'=>$jumlah_hari));
    }
}
