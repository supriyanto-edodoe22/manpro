<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ManajemenResiko extends Private_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->lang->load('auth');
    }

    public function index()
    {
        $_SESSION['id_proyek'] = $this->input->post('id_proyek');
        $data['id_proyek'] = $_SESSION['id_proyek'];
        $data['nama_proyek'] = $this->input->post('nama_proyek');
        $this->render("Proyek", "manajemen_resiko_view", $data, true, array('modules/Proyek/ManajemenResiko'));
    }

    public function grid($grid)
    {
        $this->load->model('manajemen_resiko_model');
        $model = new manajemen_resiko_model();
        echo json_encode($model->call_method($grid, 'grid'));
    }

    public function ajax($action)
    {
        $this->load->model('manajemen_resiko_model');
        $model = new manajemen_resiko_model();
        echo json_encode($model->call_method($action, 'action'));
    }
}
