<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Proyek\models\Kelola_jadwal_pekerjaan_model as PekerjaanModel;
use Illuminate\Database\Capsule\Manager as Capsule;

class KelolaJadwalPekerjaan extends Private_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $js = array(
            'dhtmlxgantt',
            'dhtmlxgantt_critical_path',
            'vis',
            'modules/Proyek/KelolaJadwalPekerjaan'
        );

        $id_proyek = $this->input->post('id_proyek');

        $data['pekerjaan'] = PekerjaanModel::where('id_proyek','=',$id_proyek)->get(['id_pekerjaan','nama','mulai','durasi']);
        $data['last_legiatan'] = PekerjaanModel::where('id_proyek','=',$id_proyek)->orderBy('mulai','DESC')->first(['id_pekerjaan']);
        $data['pekerjaan_gantt'] = PekerjaanModel::where('id_proyek','=',$id_proyek)->get(['id_pekerjaan as id','nama as text',Capsule::raw('DATE_FORMAT(mulai,\'%d-%m-%Y\') AS start_date'),'durasi as duration']);
        $data['link'] = array();
        $data['link_gantt'] = array();

        $id_link = 1;

        foreach (PekerjaanModel::where('id_proyek','=',$id_proyek)->get() as $key => $value) {
            $pendahulu = json_decode($value->pendahulu);
            if($pendahulu){                
                foreach ($pendahulu as $key_pendahulu => $value_pendahulu) {
                    $data['link'][] = array("from" => $value_pendahulu, "to" => $value->id_pekerjaan);
                    $data['link_gantt'][] = array("id" => $id_link+$key_pendahulu, "source" => $value_pendahulu, "target" => $value->id_pekerjaan, "type" => 0);
                }
            }

            $id_link++;
        }

        $css = array('dhtmlxgantt','vis');
        $this->render("Proyek", "kelola_jadwal_pekerjaan_view", $data, true, $js, $css);
    }

    public function grid($grid)
    {
        $model = new PekerjaanModel();
        echo json_encode($model->call_method($grid, 'grid'));
    }

    public function ajax($action)
    {
        $model = new PekerjaanModel();
        echo json_encode($model->call_method($action, 'action'));
    }
}
