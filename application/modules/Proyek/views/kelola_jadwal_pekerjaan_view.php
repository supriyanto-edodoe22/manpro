<style type="text/css">
    #gantt_here {width:100%; height:100%};
    .gantt_grid_head_add { visibility: hidden; }
</style>
<script type="text/javascript">
    var pekerjaan = '<?php echo json_encode($pekerjaan); ?>';
    var link_pekerjaan = '<?php echo json_encode($link); ?>';
    var pekerjaan_gantt = '<?php echo json_encode($pekerjaan_gantt); ?>';
    var link_pekerjaan_gantt = '<?php echo json_encode($link_gantt); ?>';
    var last_kegiatan = <?php echo $last_legiatan->id_pekerjaan; ?>;
</script>
 
<ul class="nav nav-tabs nav-justified">
    <li class="active"><a  href="#gantt_here" data-toggle="tab">Gantt Chart</a></li>
    <li><a href="#network_diagram" data-toggle="tab">Network Diagram</a></li>
</ul>

<div id="gantt_here" clss="gantt_wrapper panel"></div>
<div id="network_diagram"></div>

<input type="hidden" id='direction' value="UD">