<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Proyek</a>
                    </li>
                    <li class="active">Input Progress Harian</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Input Progress Harian Proyek : <?=$_SESSION['id_proyek']?> - <?=$nama_proyek?></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-md-12">
                                <a data-toggle="modal" href='#modal-id' id="modal" class="btn btn-primary pull-right"><i class="fa fa-fw fa-plus"></i>Progress Baru</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table id="list" class="table display nowrap" style="margin-bottom: -3px">
                                    <thead>
                                    <tr>
                                        <!--                                        <th rowspan="2"></th>-->
                                        <th rowspan="2">Uraian Pekerjaan</th>
                                        <th rowspan="2">Bobot %</th>
                                        <th colspan=<?php echo $jumlah_hari; ?> >Pengerjaan (%)/hari</th>
                                    </tr>
                                    <tr>
                                        <?php
                                       for ($i=1; $i<=$jumlah_hari; $i++){
                                           echo "<th>$i</th>";
                                       }

                                        ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if(count($data_pekerjaan) !== 0){
                                        $CI =& get_instance();
                                        $jumlah_bobot = $jum_bot = 0;
                                        $jumlah_bobot_harian = $jumlah_volume_harian = $actual_cost_harian = array();
                                           $no = 1;
                                        foreach ($data_pekerjaan as $key => $val){
                                            echo "<tr>";
                                            echo "<td>".$val->nama."</td>";
                                            echo "<td>".$val->bobot."</td>";
                                            $jumlah_bobot += $val->bobot;
                                            $bobot_sebelumnya = 0;
                                            $kondisi_terpenuhi = false;
                                                for ($i=1;$i<=$jumlah_hari;$i++){
//                                                    var_dump($val);
                                                    $dt = $CI->db
                                                            ->select_sum('bobot_realisasi')
                                                            ->select_sum('volume_realisasi')
                                                            ->select_sum('actual_cost')
                                                            ->where(array(
                                                                    'id_pekerjaan' => $val->id_pekerjaan,
                                                                    'periode' => $i))
                                                            ->group_by('periode')
                                                            ->get('realisasi_pekerjaan');
                                                    $res = $dt->result();

                                                    $volume_realisasi = (count($res) != 0) ? $res[0]->volume_realisasi : 0 ;
                                                    $realisasi_bobot = ($volume_realisasi/$val->volume) * 100;
                                                    $hasil_bobot = ($realisasi_bobot / 100) * $val->bobot;
                                                    $bobot = ($i == 1) ? $hasil_bobot : ($hasil_bobot > 0) ? $hasil_bobot + $bobot_sebelumnya : 0;
                                                    $kondisi_terpenuhi = ($bobot < $val->bobot) ? false : true;
                                                    $validasi_bobot = ($kondisi_terpenuhi) ? $val->bobot : $bobot;
                                                    echo "<td>".$validasi_bobot."</td>";
                                                    $bobot_sebelumnya = $bobot;
                                                    $jumlah_bobot_harian[$i] = (isset($jumlah_bobot_harian[$i])) ? $jumlah_bobot_harian[$i] + ((count($res) != 0) ? $bobot : 0) : ((count($res) != 0) ? $bobot : 0);
//                                                    $jumlah_volume_harian[$i] = (isset($jumlah_volume_harian[$i])) ? $jumlah_volume_harian[$i] + ((count($res) != 0) ? $res[0]->volume_realisasi : 0) : ((count($res) != 0) ? $res[0]->volume_realisasi : 0);
                                                    $actual_cost_harian[$i] = (isset($actual_cost_harian[$i])) ? $actual_cost_harian[$i] + ((count($res) != 0) ? $res[0]->actual_cost : 0) : ((count($res) != 0) ? $res[0]->actual_cost : 0);
                                                }
                                            echo "</tr>";
                                             //   $no++;
                                        }
                                    }else{
                                        echo "<tr><td colspan='3'>Data Tidak Ada</td></tr>";
                                    }
                                    ?>
                                    </tbody>
                                    <?php
                                    if(count($data_pekerjaan) !== 0){
                                        ?>
                                        <tr>
                                            <th>Jumlah Bobot</th>
                                            <th><?php echo isset($jumlah_bobot) ? $jumlah_bobot : 0; ?></th>
                                            <?php
                                            for ($i=1; $i<=$jumlah_hari; $i++){
                                                echo "<th>".number_format($jumlah_bobot_harian[$i],2)."</th>";
////                                                echo "<th>0</th>";
                                            }
                                            ?>
                                        </tr>
                                        <tr>
                                            <th colspan="2">Biaya Aktual</th>
                                            <?php
                                            for ($i=1; $i<=$jumlah_hari; $i++) {
//                                                echo "<th>0</th>";
                                                echo "<th>".$actual_cost_harian[$i]."</th>";
                                            }
                                            ?>
                                        </tr>
<!--                                         <tfoot>
                                        </tfoot> -->
                                        <?php
                                    }
                                    ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal-id">
    <div class="modal-dialog">
        <form id="form" action="<?php echo base_url('Proyek/InputProgress/ajax/insert'); ?>" method="POST" class="form-horizontal" role="form" onsubmit="doAction(this)">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Form Input Progress</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" class="primary" name="id_realisasi">
                    <input type="hidden" class="primary" name="tipe" value="1">
                    <div class="form-group">
                        <label class="col-sm-4">Hari Ke</label>
                        <div class="col-sm-8">
                            <select name="tanggal" id="tanggal" class="find-on-select" style="width: 75%">
                                <?php
                                if(isset($periode_harian)){
                                    foreach ($periode_harian as $i => $v) {
                                        echo "<option value='".($i+1)."_".$v."'>".($i+1)." (".$v.") </option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Pekerjaan</label>
                        <div class="col-sm-8">
                            <select name="id_pekerjaan" id="id_pekerjaan" class="find-on-select" style="width: 75%">
                                <?php
                                if(isset($data_pekerjaan)){
                                    foreach ($data_pekerjaan as $item => $value) {
                                        echo "<option value='".$value->id_pekerjaan."'>".$value->nama."</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Satuan</label>
                        <div class="col-sm-8">
                            <select name="satuan" id="satuan" class="form-control">
                                <option value="Kaleng (Kaleng)">Kaleng (Kaleng)</option>
                                <option value="Kg (Kg)">Kg (Kg)</option>
                                <option value="Batang (Batang)">Batang (Batang)</option>
                                <option value="bh (bh)">bh (bh)</option>
                                <option value="biji (biji)">biji (biji)</option>
                                <option value="bks (bks)">bks (bks)</option>
                                <option value="Botol (Botol)">Botol (Botol)</option>
                                <option value="cm (cm)">cm (cm)</option>
                                <option value="cm2 (cm2)">cm2 (cm2)</option>
                                <option value="daun (daun)">daun (daun)</option>
                                <option value="drum (drum)">drum (drum)</option>
                                <option value="dus (dus)">dus (dus)</option>
                                <option value="hari (hari)">hari (hari)</option>
                                <option value="ikat (ikat)">ikat (ikat)</option>
                                <option value="Item (Item)">Item (Item)</option>
                                <option value="jam (jam)">jam (jam)</option>
                                <option value="lbr (lbr)">lbr (lbr)</option>
                                <option value="Liter (Liter)">Liter (Liter)</option>
                                <option value="lonjor (lonjor)">lonjor (lonjor)</option>
                                <option value="ls (ls)">ls (ls)</option>
                                <option value="m (m)">m (m)</option>
                                <option value="m1 (m1)">m1 (m1)</option>
                                <option value="m2 (m2)">m2 (m2)</option>
                                <option value="m3 (m3)">m3 (m3)</option>
                                <option value="Org (Org)">Org (Org)</option>
                                <option value="Org/Hari (Org/Hari)">Org/Hari (Org/Hari)</option>
                                <option value="paket (paket)">paket (paket)</option>
                                <option value="pcs (pcs)">pcs (pcs)</option>
                                <option value="Persen (%)">Persen (%)</option>
                                <option value="roll (roll)">roll (roll)</option>
                                <option value="sak (sak)">sak (sak)</option>
                                <option value="sewa-hari (uh)">sewa-hari (uh)</option>
                                <option value="sewa-jam (uj)">sewa-jam (uj)</option>
                                <option value="ton (ton)">ton (ton)</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Volume</label>
                        <div class="col-sm-8">
                            <input type="number" name="volume_realisasi" id="volume_realisasi" class="form-control" value="0" min="{0}" required="required" title="Volume Realisasi">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Biaya Aktual</label>
                        <div class="col-sm-8">
                            <input type="number" name="actual_cost" id="actual_cost" class="form-control" value="0" min="{0}" required="required" title="Biaya Aktual">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="modal" id="delete-confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Anda yakin?</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="doAction(this)">Ya</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
            </div>
        </div>
    </div>
</div>