<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Proyek</a>
                    </li>
                    <li class="active">Input Progress Mingguan</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Input Progress Mingguan Proyek : <?=$_SESSION['id_proyek']?> - <?=$nama_proyek?></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-md-12">
                                <a data-toggle="modal" href='#modal-id' id="modal" class="btn btn-primary pull-right"><i class="fa fa-fw fa-plus"></i>Progress Baru</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table id="list" class="table table-bordered table-condensed table-hover table-striped">
                                    <thead>
                                    <tr>
<!--                                        <th rowspan="2"></th>-->
                                        <th rowspan="2">Uraian Pekerjaan</th>
                                        <th rowspan="2">Bobot %</th>
                                        <th colspan="6">Pekerjaan Minggu Ke-</th>
                                    </tr>
                                    <tr>
                                        <?php
                                            foreach ($tanggal_minggu as $key => $val){
                                                echo "<th>".($key + 1)."</th>";
                                            }
                                        ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        if(count($data_pekerjaan) !== 0){
                                            $CI =& get_instance();
                                            $jumlah_bobot = $jum_bot = 0;
                                            $jumlah_bobot_mingguan = array();
//                                            $no = 1;
                                            foreach ($data_pekerjaan as $key => $val){
                                                echo "<tr>";
//                                                echo "<td>".$no."</td>";
                                                echo "<td>".$val->nama."</td>";
                                                echo "<td>".$val->bobot."</td>";
                                                $jumlah_bobot += $val->bobot;
//                                                for ($i=1;$i<=$jumlah_minggu;$i++){
                                                foreach ($tanggal_minggu as $k => $v){
                                                    $dt = $CI->db->where(array('id_pekerjaan' => $val->id_pekerjaan, 'dari'=> $v['awal'], 'sampai' => $v['akhir']))->get('realisasi_pekerjaan');
                                                    $res = $dt->result();
                                                    echo "<th>".((count($res) != 0) ? $res[0]->bobot_realisasi : 0)."</th>";
                                                    $jumlah_bobot_mingguan[$k] = (isset($jumlah_bobot_mingguan[$k])) ? $jumlah_bobot_mingguan[$k] + ((count($res) != 0) ? $res[0]->bobot_realisasi : 0) : ((count($res) != 0) ? $res[0]->bobot_realisasi : 0);
                                                }
                                                echo "</tr>";
//                                                $no++;
                                            }
                                        }else{
                                            echo "<tr><td colspan='3'>Data Tidak Ada</td></tr>";
                                        }
                                    ?>
                                    </tbody>
                                    <?php
                                        if(count($data_pekerjaan) !== 0){
                                    ?>
                                    <tfoot>
                                    <tr>
                                        <th>Jumlah Bobot</th>
                                        <th><?=isset($jumlah_bobot) ? $jumlah_bobot : 0?></th>
                                        <?php
                                            foreach ($tanggal_minggu as $k => $v){
                                                echo "<th>".$jumlah_bobot_mingguan[$k]."</th>";
                                            }
                                        ?>
                                    </tr>
                                    </tfoot>
                                    <?php
                                        }
                                    ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal-id">
    <div class="modal-dialog">
        <form id="form" action="<?php echo base_url('Proyek/InputProgress/ajax/insert'); ?>" method="POST" class="form-horizontal" role="form" onsubmit="doAction(this)">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Form Input Progress</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" class="primary" name="id_realisasi">
                    <input type="hidden" class="primary" name="tipe" value="2">
                    <div class="form-group">
                        <label class="col-sm-4">Minggu Ke</label>
                        <div class="col-sm-8">
                            <select name="tanggal" id="tanggal" class="find-on-select" style="width: 75%">
                                <?php
                                if(isset($tanggal_minggu)){
                                    foreach ($tanggal_minggu as $i => $v) {
                                        echo "<option value='".$v['awal']."_".$v['akhir']."'>".($i+1)." (".$v['awal']." s/d ".$v['akhir'].") </option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Pekerjaan</label>
                        <div class="col-sm-8">
                            <select name="id_pekerjaan" id="id_pekerjaan" class="find-on-select" style="width: 75%">
                                <?php
                                if(isset($data_pekerjaan)){
                                    foreach ($data_pekerjaan as $item => $value) {
                                        echo "<option value='".$value->id_pekerjaan."'>".$value->nama."</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
<!--                    <div class="form-group">-->
<!--                        <label class="col-sm-4">Satuan</label>-->
<!--                        <div class="col-sm-8">-->
<!--                            <select name="satuan" id="satuan" class="form-control">-->
<!--                                <option value="Kaleng (Kaleng)">Kaleng (Kaleng)</option>-->
<!--                                <option value="Kg (Kg)">Kg (Kg)</option>-->
<!--                                <option value="Batang (Batang)">Batang (Batang)</option>-->
<!--                                <option value="bh (bh)">bh (bh)</option>-->
<!--                                <option value="biji (biji)">biji (biji)</option>-->
<!--                                <option value="bks (bks)">bks (bks)</option>-->
<!--                                <option value="Botol (Botol)">Botol (Botol)</option>-->
<!--                                <option value="cm (cm)">cm (cm)</option>-->
<!--                                <option value="cm2 (cm2)">cm2 (cm2)</option>-->
<!--                                <option value="daun (daun)">daun (daun)</option>-->
<!--                                <option value="drum (drum)">drum (drum)</option>-->
<!--                                <option value="dus (dus)">dus (dus)</option>-->
<!--                                <option value="hari (hari)">hari (hari)</option>-->
<!--                                <option value="ikat (ikat)">ikat (ikat)</option>-->
<!--                                <option value="Item (Item)">Item (Item)</option>-->
<!--                                <option value="jam (jam)">jam (jam)</option>-->
<!--                                <option value="lbr (lbr)">lbr (lbr)</option>-->
<!--                                <option value="Liter (Liter)">Liter (Liter)</option>-->
<!--                                <option value="lonjor (lonjor)">lonjor (lonjor)</option>-->
<!--                                <option value="ls (ls)">ls (ls)</option>-->
<!--                                <option value="m (m)">m (m)</option>-->
<!--                                <option value="m1 (m1)">m1 (m1)</option>-->
<!--                                <option value="m2 (m2)">m2 (m2)</option>-->
<!--                                <option value="m3 (m3)">m3 (m3)</option>-->
<!--                                <option value="Org (Org)">Org (Org)</option>-->
<!--                                <option value="Org/Hari (Org/Hari)">Org/Hari (Org/Hari)</option>-->
<!--                                <option value="paket (paket)">paket (paket)</option>-->
<!--                                <option value="pcs (pcs)">pcs (pcs)</option>-->
<!--                                <option value="Persen (%)">Persen (%)</option>-->
<!--                                <option value="roll (roll)">roll (roll)</option>-->
<!--                                <option value="sak (sak)">sak (sak)</option>-->
<!--                                <option value="sewa-hari (uh)">sewa-hari (uh)</option>-->
<!--                                <option value="sewa-jam (uj)">sewa-jam (uj)</option>-->
<!--                                <option value="ton (ton)">ton (ton)</option>-->
<!--                            </select>-->
<!--                        </div>-->
<!--                    </div>-->
                    <div class="form-group">
                        <label class="col-sm-4">Bobot</label>
                        <div class="col-sm-8">
                            <input type="number" name="bobot_realisasi" id="bobot_realisasi" class="form-control" value="0" min="{1}" required="required" title="Bpbpt Realisasi">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="modal" id="delete-confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Anda yakin?</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="doAction(this)">Ya</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
            </div>
        </div>
    </div>
</div>