<style type="text/css">
    .pagination {
        float: right;
    }

    td.details-control {
        background: url('../../assets/images/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.details td.details-control {
        background: url('../../assets/images/details_close.png') no-repeat center center;
    }
</style>

<script type="text/javascript">
    var data_pekerjaan = <?php echo json_encode($data_pekerjaan); ?>;
    var material = <?php echo json_encode($material); ?>;
    var peralatan = <?php echo json_encode($peralatan); ?>;
    var pekerja = <?php echo json_encode($pekerja); ?>;
</script>

<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Proyek</a>
                    </li>
                    <li class="active">Kelola Data RAB</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" draggable="true">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Kelola Data RAB</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-md-12">
                                <a data-toggle="modal" href='#modal-id' id="modal" class="btn btn-primary pull-right"><i class="fa fa-fw fa-plus"></i>RAB Baru</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table id="list_rab" class="table table-bordered table-condensed table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>No</th>
                                        <th>Uraian</th>
                                        <!-- <th>Satuan</th> -->
                                        <th>Perkiraan Kuantitas</th>
                                        <!-- <th>Harga Satuan</th> -->
                                        <th>Total</th>
                                        <th>Aksi</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th colspan="4" style="text-align:right">Pajak 10%:</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <th colspan="4" style="text-align:right">Total:</th>
                                        <th class="total"></th>
                                        <th></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal-id">
    <div class="modal-dialog modal-lg">
        <form id="form" action="<?php echo current_url() . "/ajax/insert/"; ?>" method="POST" class="form-horizontal" role="form" onsubmit="doAction(this)">
        <input type="hidden" class="primary" name="header[id_rab]">
        <input type="hidden" id="id_proyek" name="header[id_proyek]" value="<?php echo $id_proyek; ?>" >
        <input type="hidden" id="id_pekerjaan" name="header[id_pekerjaan]" >
        <input type="hidden" id="total" name="header[total]" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Form Data RAB</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-sm-4">Uraian</label>
                        <div class="col-sm-8">
                            <input type="text" id="nama_pekerjaan" class="form-control auto_rab" auto-type="pekerjaan" required="required" title="Nama Pekerjan" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <table id="child_rab" class="table table-bordered table-condensed table-hover table-striped display">
                                <thead>
                                <tr>
                                    <th>Jenis</th>
                                    <th>Deskripsi</th>
                                    <th>Satuan</th>
                                    <th>Kuantitas</th>
                                    <th>Harga Satuan</th>
                                    <th>Total</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
<!--                     <div class="form-group">
                        <label class="col-sm-4">Satuan</label>
                        <div class="col-sm-8">
                            <select name="satuan" id="satuan" class="form-control">
                                <option value="Kaleng (Kaleng)">Kaleng (Kaleng)</option>
                                <option value="Kg (Kg)">Kg (Kg)</option>
                                <option value="Batang (Batang)">Batang (Batang)</option>
                                <option value="bh (bh)">bh (bh)</option>
                                <option value="biji (biji)">biji (biji)</option>
                                <option value="bks (bks)">bks (bks)</option>
                                <option value="Botol (Botol)">Botol (Botol)</option>
                                <option value="cm (cm)">cm (cm)</option>
                                <option value="cm2 (cm2)">cm2 (cm2)</option>
                                <option value="daun (daun)">daun (daun)</option>
                                <option value="drum (drum)">drum (drum)</option>
                                <option value="dus (dus)">dus (dus)</option>
                                <option value="hari (hari)">hari (hari)</option>
                                <option value="ikat (ikat)">ikat (ikat)</option>
                                <option value="Item (Item)">Item (Item)</option>
                                <option value="jam (jam)">jam (jam)</option>
                                <option value="lbr (lbr)">lbr (lbr)</option>
                                <option value="Liter (Liter)">Liter (Liter)</option>
                                <option value="lonjor (lonjor)">lonjor (lonjor)</option>
                                <option value="ls (ls)">ls (ls)</option>
                                <option value="m (m)">m (m)</option>
                                <option value="m1 (m1)">m1 (m1)</option>
                                <option value="m2 (m2)">m2 (m2)</option>
                                <option value="m3 (m3)">m3 (m3)</option>
                                <option value="Org (Org)">Org (Org)</option>
                                <option value="Org/Hari (Org/Hari)">Org/Hari (Org/Hari)</option>
                                <option value="paket (paket)">paket (paket)</option>
                                <option value="pcs (pcs)">pcs (pcs)</option>
                                <option value="Persen (%)">Persen (%)</option>
                                <option value="roll (roll)">roll (roll)</option>
                                <option value="sak (sak)">sak (sak)</option>
                                <option value="sewa-hari (uh)">sewa-hari (uh)</option>
                                <option value="sewa-jam (uj)">sewa-jam (uj)</option>
                                <option value="ton (ton)">ton (ton)</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Kuantitas</label>
                        <div class="col-sm-8">
                            <input type="text" name="kuantitas" id="kuantitas" class="form-control" value="" min="{0}" required="required" title="kuantitas">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Harga Satuan</label>
                        <div class="col-sm-8">
                            <input type="text" name="harga" id="harga" class="form-control" value="" min="{0}" required="required" title="kuantitas">
                        </div>
                    </div> -->
<!--                     <div class="form-group">
                        <label class="col-sm-4">Total</label>
                        <div class="col-sm-8">
                            <input type="text" name="total" id="total" class="form-control" value="" min="{0}" required="required" title="kuantitas">
                        </div>
                    </div> -->
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="modal" id="delete-confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Anda yakin?</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="doAction(this)">Ya</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
            </div>
        </div>
    </div>
</div>