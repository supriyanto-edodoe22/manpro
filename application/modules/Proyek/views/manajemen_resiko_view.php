<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Proyek</a>
                    </li>
                    <li class="active">Manajemen Resiko</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" draggable="true">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Manajemen Resiko Proyek : <?=$_SESSION['id_proyek']?> - <?=$nama_proyek?></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-md-12">
                                <a data-toggle="modal" href='#modal-id' id="modal" class="btn btn-primary pull-right"><i class="fa fa-fw fa-plus"></i>Resiko Baru</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table id="list_resiko" class="table table-bordered table-condensed table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode Resiko</th>
                                        <th>Deskripsi</th>
                                        <th>Probabilitas</th>
                                        <th>Dampak</th>
                                        <th>Nilai Kepentingan</th>
                                        <th>Tingkat Kepentingan</th>
                                        <th>Mitigasi</th>
                                        <th>Aksi</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal-id">
    <div class="modal-dialog">
        <form id="form" action="<?php echo current_url() . "/ajax/insert/"; ?>" method="POST" class="form-horizontal" role="form" onsubmit="doAction(this)">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Form Data Resiko</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" class="primary" name="kode_resiko">
                    <input type="hidden" name="id_proyek" value="<?=$id_proyek?>">
                    <div class="form-group">
                        <label class="col-sm-4">kode</label>
                        <div class="col-sm-8">
                            <input type="text" name="kode_resiko" id="kode_resiko" class="form-control" value="" min="{0}" required="required" title="Kode Resiko">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Deskripsi</label>
                        <div class="col-sm-8">
                            <input type="text" name="deskripsi" id="deskripsi" class="form-control" value="" min="{0}" required="required" title="Deskrispi">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Probabilitas</label>
                        <div class="col-sm-8">
                            <input type="text" name="probabilitas" id="probabilitas" class="form-control" value="" min="{0}" required="required" title="Probabilitas">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Dampak</label>
                        <div class="col-sm-8">
                            <input type="text" name="dampak" id="dampak" class="form-control" value="" min="{0}" required="required" title="Dampak">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Nilai Kepentingan</label>
                        <div class="col-sm-8">
                            <input type="text" name="nilai_kepentingan" id="nilai_kepentingan" class="form-control" value="" min="{0}" required="required" title="Nilai Kepentingan">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Tingkat Kepentingan</label>
                        <div class="col-sm-8">
                            <input type="text" name="tingkat_kepentingan" id="tingkat_kepentingan" class="form-control" value="" min="{0}" required="required" title="Tingkat Kepentingan">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Mitigasi</label>
                        <div class="col-sm-8">
                            <input type="text" name="mitigasi" id="mitigasi" class="form-control" value="" min="{0}" required="required" title="Mitigasi">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="modal" id="delete-confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Anda yakin?</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="doAction(this)">Ya</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
            </div>
        </div>
    </div>
</div>