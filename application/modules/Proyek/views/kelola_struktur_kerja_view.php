<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Proyek</a>
                    </li>
                    <li class="active">Kelola Struktur Kerja</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Kelola Struktur Kerja Proyek : <?=$_SESSION['id_proyek']?> - <?=$nama_proyek?></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-md-12">
                                <a data-toggle="modal" href='#modal-id' id="modal" class="btn btn-primary pull-right"><i class="fa fa-fw fa-plus"></i>Buat Struktur Kerja</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table id="list_struktur_kerja" class="table table-bordered table-condensed table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Durasi</th>
                                        <th>Bobot</th>
                                        <th>Volume</th>
                                        <th>Mulai</th>
                                        <th>Selesai</th>
                                        <th>Aksi</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal-id">
    <div class="modal-dialog">
        <form id="form" action="<?php echo current_url() . "/ajax/insert/"; ?>" method="POST" class="form-horizontal" role="form" onsubmit="doAction(this)">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Form Struktur Kerja</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" class="primary" name="id_pekerjaan">
                    <input type="hidden" name="id_proyek" value="<?php echo $id_proyek;?>">
                    <div class="form-group">
                        <label class="col-sm-4">Pendahulu</label>
                        <div class="col-sm-8">
                            <select name="pendahulu[]" id="pendahulu" class="multiselect-filter" multiple="multiple">
                                <?php
                                    if(isset($data_pekerjaan)){
                                        foreach ($data_pekerjaan as $item => $value) {
                                            echo "<option value='".$value->id_pekerjaan."'>".$value->nama."</option>";
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Nama</label>
                        <div class="col-sm-8">
                            <input type="text" name="nama" id="nama" class="form-control" value="" min="{0}" required="required" title="Nama Pekerjaan">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Mulai</label>
                        <div class="col-sm-8">
                            <input type="date" name="mulai" id="mulai" class="form-control" value="" min="{0}" required="required" title="Tanggal di mulai pekerjaan" onblur="cal_durasi()">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Selesai</label>
                        <div class="col-sm-8">
                            <input type="date" name="selesai" id="selesai" class="form-control" value="" min="{0}" required="required" title="Tanggal selesai pekerjaan" onblur="cal_durasi()">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Durasi</label>
                        <div class="col-sm-8">
                            <input type="text" name="durasi" id="durasi" class="form-control" value="" min="{0}" required="required" title="Durasi Pekerjaan" readonly="readonly">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Bobot</label>
                        <div class="col-sm-8">
                            <input type="text" name="bobot" id="bobot" class="form-control" value="" min="{0}" required="required" title="Bobot Pekerjaan">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Volume</label>
                        <div class="col-sm-8">
                            <input type="text" name="volume" id="volume" class="form-control" value="" min="{0}" required="required" title="Volume Pekerjaan">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="modal" id="delete-confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Anda yakin?</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="doAction(this)">Ya</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
            </div>
        </div>
    </div>
</div>