<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Proyek</a>
                    </li>
                    <li class="active">Kelola Rencana Kerja</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Kelola Rencana Kerja Proyek : <?php echo $_SESSION['id_proyek']; ?>
                            - <?php echo $nama_proyek; ?></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="list" class="table display nowrap" style="margin-bottom: -3px">
                                    <thead>
                                    <tr>
                                        <!--                                        <th rowspan="2"></th>-->
                                        <th rowspan="2">Uraian Pekerjaan</th>
                                        <th rowspan="2">Bobot %</th>
                                        <th colspan="<?php echo $jumlah_hari; ?>">Pekerjaan Hari Ke-</th>
                                    </tr>
                                    <tr>
                                        <?php
                                        for ($i = 1; $i <= $jumlah_hari; $i++) {
                                            echo "<th>$i</th>";
                                        }
                                        ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (count($data_pekerjaan) !== 0) {
                                        $CI =& get_instance();
                                        $jumlah_bobot = $jum_bot = 0;
                                        $jumlah_bobot_harian = array();
                                        //                                            $no = 1;
                                        foreach ($data_pekerjaan as $key => $val) {
                                            echo "<tr>";
                                            echo "<td>" . $val->nama . "</td>";
                                            echo "<td>" . $val->bobot . "</td>";
                                            $jumlah_bobot += $val->bobot;
                                            $bobot_sebelumnya = 0;
                                            for ($i = 1; $i <= $jumlah_hari; $i++) {
                                                $bobot = (($i==1) ? ($val->bobot + $bobot_sebelumnya) / $val->durasi : ($i <= $val->durasi) ? ($val->bobot / $val->durasi) + $bobot_sebelumnya : $val->bobot ) ;
                                                echo "<th>" . number_format($bobot, 2) . "</th>";
                                                $jumlah_bobot_harian[$i] = (isset($jumlah_bobot_harian[$i])) ? $jumlah_bobot_harian[$i] + $bobot  : $bobot;
                                                $bobot_sebelumnya = number_format($bobot,2);
                                            }
                                            echo "</tr>";
                                            //                                                $no++;
                                        }
                                    } else {
                                        echo "<tr><td colspan='" . (count($tanggal_minggu) + 2) . "'>Data Tidak Ada</td></tr>";
                                    }
                                    ?>
                                    <tr>
                                        <th>Jumlah Bobot</th>
                                        <th><?php echo isset($jumlah_bobot) ? $jumlah_bobot : 0 ?></th>
                                        <?php
                                        for ($i=1;$i<=$jumlah_hari;$i++){
                                            echo "<th>" . number_format($jumlah_bobot_harian[$i],2) . "</th>";
                                        }
                                        ?>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>