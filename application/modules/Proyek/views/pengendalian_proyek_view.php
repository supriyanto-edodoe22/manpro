<script type="text/javascript">
    var jumlah_hari = '<?php echo json_encode($jumlah_hari); ?>';
    var pekerjaan = '<?php echo json_encode($pekerjaan); ?>';
    var link_pekerjaan = '<?php echo json_encode($link); ?>';
    var pekerjaan_gantt = '<?php echo json_encode($pekerjaan_gantt); ?>';
    var link_pekerjaan_gantt = '<?php echo json_encode($link_gantt); ?>';
    var last_kegiatan = <?php echo $last_legiatan->id_pekerjaan; ?>;
</script>
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Proyek</a>
                    </li>
                    <li class="active">Pengendalian Proyek</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Pengendalian Proyek : <?=$_SESSION['id_proyek']?> - <?=$nama_proyek?></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="list" class="table table-bordered table-condensed table-hover table-striped display">
                                    <thead>
                                    <tr>
                                        <th rowspan="2">Hari</th>
                                        <th rowspan="2">SV</th>
                                        <th colspan="6">CV</th>
                                        <th colspan="6">SPI</th>
                                        <th colspan="6">CPI</th>
                                        <th colspan="6">ETC</th>
                                        <th colspan="6">EAC</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id='direction' value="UD">