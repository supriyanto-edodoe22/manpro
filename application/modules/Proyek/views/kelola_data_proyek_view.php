<style>
    .dropdown-menu {
        width: 190px;
    }
</style>
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Proyek</a>
                    </li>
                    <li class="active">Kelola Data Proyek</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Kelola Data Proyek</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-md-12">
                                <a data-toggle="modal" href='#modal-id' id="modal" class="btn btn-primary pull-right"><i class="fa fa-fw fa-plus"></i>Project Baru</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table id="list_proyek" class="table table-bordered table-condensed table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Info</th>
                                        <th>Progress</th>
                                        <th>Aksi</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal-id">
    <div class="modal-dialog">
        <form id="form" action="<?php echo current_url() . "/ajax/insert/"; ?>" method="POST" class="form-horizontal" role="form" onsubmit="doAction(this)">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Form Data Proyek</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" class="primary" name="id_proyek">
                    <div class="form-group">
                        <label class="col-sm-4">Nama</label>
                        <div class="col-sm-8">
                            <input type="text" name="nama" id="nama" class="form-control" value="" min="{0}" required="required" title="Nama Proyek">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Lokasi</label>
                        <div class="col-sm-8">
                            <input type="text" name="lokasi" id="lokasi" class="form-control" value="" min="{0}" required="required" title="Lokasi Proyek">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Pemilik</label>
                        <div class="col-sm-8">
                            <input type="text" name="pemilik" id="pemilik" class="form-control" value="" min="{0}" required="required" title="Pemilik Proyek">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Jenis</label>
                        <div class="col-sm-8">
                            <select name="jenis" id="jenis" class="form-control">
                                <option value="Aktifitas Harian">Aktifitas Harian</option>
                                <option value="Irigasi dan Bangunan Air">Irigasi dan Bangunan Air</option>
                                <option value="Jalan dan Jembatan">Jalan dan Jembatan</option>
                                <option value="Lain-lain">Lain-lain</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Tanggal Kontrak</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <input type="date" name="tgl_kontrak" id="tgl_kontrak" class="form-control" value="" required="required" title="Tanggal Kontrak Proyek">
                                <div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Nilai Kontrak</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <div class="input-group-addon">Rp </div>
                                <input type="number" step="any" name="nilai" id="nilai" class="form-control" value="" required="required" title="Nilai Kontrak">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Tanggal Mulai</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <input type="date" name="tgl_mulai" id="tgl_mulai" class="form-control" value="" required="required" title="Tanggal Mulai Proyek">
                                <div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Tanggal Selesai</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <input type="date" name="tgl_selesai" id="tgl_selesai" class="form-control" value="" required="required" title="Tanggal Selesai Proyek">
                                <div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Durasi</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
                                <input type="number" name="durasi" id="durasi" class="form-control" value="" required="required" title="Proyek Proyek">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Deskripsi</label>
                        <div class="col-sm-8">
                            <textarea name="deskripsi" id="deskripsi" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Catatan</label>
                        <div class="col-sm-8">
                            <textarea name="catatan" id="catatan" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Mandor</label>
                        <div class="col-sm-8">
                            <select name="user_id" id="user_id" class="form-control">
                                <?php
                                if (isset($mandor))
                                    foreach ($mandor as $key => $val) {
                                        echo "<option value='" . $val->id . "'>" . $val->nama . "</option>";
                                    }
                                else
                                    echo "<option>Saat ini mandor tidak tersedia</option>";
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="reset" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </form>
    </div>
</div>

<form id="target" method="POST" style="visibility: hidden">
    <input type="text" name="id_proyek" value="" />
    <input type="text" name="nama_proyek" value="" />
</form>