<?php
namespace Konfigurasi\models;
defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;

class Kelola_harga_peralatan_model extends Eloquent
{
    protected $table = "peralatan";
    protected $fillable = ['*'];
    public $timestamps = false;
    protected $data = array();
    protected $appends = array();
    protected $return = array();
    protected $res = array('status' => false, 'message' => 'Error');

    //call_method Model
    public function call_method($method, $type = '')
    {
        $this->$method();

        return $this->res;
    }

    public function list_peralatan()
    {
        $columns = $_GET['columns'];
        $search = $_GET['search']['value'];
        $get_data = Eloquent::select("*");

        $val = $_GET['search']['value'];
        if (!empty($val))
            $get_data->where(function ($ds) use ($columns, $search) {
                foreach ($columns as $i => $v) {
                    if (!empty($v['data']) && $v['searchable'] == 'true')
                        $ds->orWhere($v['data'], 'LIKE', '%' . $search . '%');
                }
            });

        // order
        foreach ($_GET['order'] as $i => $o) {
            $get_data->orderBy($columns[$o['column']]['data'], $o['dir']);
        }

        $this->data = $get_data->take($_GET['length'])->offset($_GET['start'])->get();
        $i = 1;
        foreach ($this->data as $key => $value) {
            $value->no = $i;
            $value->aksi = "<i class=\"fa fa-pencil fa-fw text-primary\" style='cursor: pointer'></i>";
            $value->aksi .= "<i class=\"fa fa-trash fa-fw text-primary\" style='cursor: pointer'></i>";
            $i++;
        }


        $val = $_GET['search']['value'];
        if (!empty($val))
            $count = Eloquent::where(function ($ds) use ($columns, $search) {
                foreach ($columns as $i => $v) {
                    if (!empty($v['data']) && $v['searchable'] == 'true')
                        $ds->orWhere($v['data'], 'LIKE', '%' . $search . '%');
                }
            });


        $this->res = array(
            'recordsTotal' => isset($count) ? $count->count() : Eloquent::count(),
            'recordsFiltered' => isset($count) ? $count->count() : Eloquent::count(),
            'data' => $this->data
        );

        return $this->res;
    }

    public function insert()
    {
        $Kelola_harga_peralatan_model = new Kelola_harga_peralatan_model();
        //hapus kiriman primary table dari POST
        unset($_POST['id_peralatan']);
        foreach ($_POST as $key => $val){
            $Kelola_harga_peralatan_model->$key = $val;
        }

        if ($Kelola_harga_peralatan_model->save()) {
            $this->res = array('status' => true, 'message' => 'Success');
        }
    }

    public function edit()
    {
        if(Eloquent::where('id_peralatan', '=', $_POST['id_peralatan'])->update($_POST))
            $this->res = array('status' => true, 'message' => 'Success');
    }

    public function delete()
    {
        if(Eloquent::where('id_peralatan', '=', $_POST['id'])->delete())
            $this->res = array('status' => true, 'message' => 'Success');
    }
}
