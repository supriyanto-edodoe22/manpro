<?php
namespace Konfigurasi\models;
defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;

class Pengguna_group_model extends Eloquent
{
    protected $table = "users_groups";
    protected $fillable = ['*'];
    public $timestamps = false;

}
