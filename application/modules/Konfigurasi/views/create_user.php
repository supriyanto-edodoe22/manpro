<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li>
                        <a href="#">Konfigurasi</a>
                    </li>
                    <li class="active">Kelola Data Pengguna</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" draggable="true">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Kelola Data Pengguna</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-md-12">
                                <a data-toggle="modal" href='#modal-id' id="modal" class="btn btn-primary pull-right"><i class="fa fa-fw fa-plus"></i>Baru</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table id="list_pengguna" class="table table-bordered table-condensed table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Nama</th>
                                        <th>Jabatan</th>
                                        <th>No Tlp</th>
                                        <th>Email</th>
                                        <th>Aksi</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal-id">
    <div class="modal-dialog">
        <form id="form" action="<?php echo current_url() . "/ajax/insert/"; ?>" method="POST" class="form-horizontal" role="form" onsubmit="doAction(this)">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Form Data Proyek</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" class="primary" name="id">
                    <div class="form-group">
                        <label class="col-sm-4">Nama</label>
                        <div class="col-sm-8">
                            <input type="text" name="nama" id="nama" class="form-control" value="" min="{0}" required="required" title="Nama Proyek">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Email</label>
                        <div class="col-sm-8">
                            <input type="email" name="email" id="nama" class="form-control" value="" min="{0}" required="required" title="Nama Proyek">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">No Telp</label>
                        <div class="col-sm-8">
                            <input type="phone" name="phone" id="phone" class="form-control" value="" min="{0}" title="No Telephone">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Jabatan</label>
                        <div class="col-sm-8">
                            <select name="group_id" id="group_id" class="form-control">
                                <option value="1">Admin</option>
                                <option value="2">Mandor</option>
                                <option value="3">Pelaksana Teknis</option>
                                <option value="4">Penanggung Jawab Teknis</option>
                                <option value="5">Direktur</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Username</label>
                        <div class="col-sm-8">
                            <input type="text" name="username" id="username" class="form-control" value="" min="{3}" required="required" title="username">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Password</label>
                        <div class="col-sm-8">
                            <input type="password" name="password" id="password" class="form-control" value="" min="{6}" required="required" title="password">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="modal" id="delete-confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Anda yakin?</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="doAction(this)">Ya</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
            </div>
        </div>
    </div>
</div>
