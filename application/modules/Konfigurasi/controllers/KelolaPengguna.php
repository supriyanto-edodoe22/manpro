<?php defined('BASEPATH') OR exit('No direct script access allowed');

class KelolaPengguna extends Private_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->lang->load('auth');
    }

    public function index()
    {
        $identity_column = $this->config->item('identity','ion_auth');
        $this->data['identity_column'] = $identity_column;

        $this->render("Konfigurasi", "create_user", $this->data, true, array('modules/Konfigurasi/KelolaPengguna'));
    }

    public function grid($grid)
    {
        $pengguna = new \Konfigurasi\models\Pengguna_model();
        echo json_encode($pengguna->call_method($grid, 'grid'));
    }

    public function ajax($action)
    {
        $pengguna = new \Konfigurasi\models\Pengguna_model();
        echo json_encode($pengguna->call_method($action, 'action'));
    }
}
