<?php defined('BASEPATH') OR exit('No direct script access allowed');

class KelolaHargaPekerja extends Private_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->lang->load('auth');
    }

    public function index()
    {
        $this->render("Konfigurasi", "kelola_harga_pekerja_view", $this->data, true, array('modules/Konfigurasi/KelolaHargaPekerja'));
    }

    public function grid($grid)
    {
        $model = new \Konfigurasi\models\Kelola_harga_pekerja_model();
        echo json_encode($model->call_method($grid, 'grid'));
    }

    public function ajax($action)
    {
        $model = new \Konfigurasi\models\Kelola_harga_pekerja_model();
        echo json_encode($model->call_method($action, 'action'));
    }
}
