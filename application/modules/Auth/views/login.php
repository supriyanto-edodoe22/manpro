<div class="container">
  <?php echo form_open("auth/login","class='form-signin'");?>
    <div class="form-signin-heading" style="padding: 0 45px 0 45px;"><img src="<?php echo base_url('assets/images/company-logo.png'); ?>" class="img-responsive" alt="Responsive image"></div>
    <div id="infoMessage"><?php echo $message;?></div>
    <?php echo lang('login_identity_label', 'identity', 'class="sr-only"');?>
    <?php echo form_input($identity,'','class="form-control" placeholder="Username" required autofocus');?>
    <?php echo lang('login_password_label', 'password', 'class="sr-only"');?>
    <?php echo form_input($password,'','class="form-control" placeholder="Password" required');?>
    <div class="checkbox">
      <label>
        <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?> Remember me
      </label>
    </div>
    <?php echo form_submit('submit', lang('login_submit_btn'),'class="btn btn-lg btn-primary btn-block"');?>
  <?php echo form_close();?>
</div>