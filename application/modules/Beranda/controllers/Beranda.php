<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends Private_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->render("Beranda", "beranda_view");
    }
}
	