<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use MyLibraries\Libraries\Template as Template;
use Illuminate\Database\Capsule\Manager as Capsule;

class Private_Controller extends MX_Controller
{

    protected $data;
    protected $primary;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library(array('ion_auth'));

        if (!$this->ion_auth->logged_in())
            redirect('auth/login');
        elseif (!$this->check_module())
            show_404();
    }

    public function check_module()
    {
        $method_pass = array('grid', 'ajax');
        if (in_array($this->router->fetch_method(), $method_pass) == false) {
            $this->load->model('ion_auth_model');
            $group = $this->ion_auth_model->get_users_groups()->row()->id;
            $path = explode('/', $this->router->directory);
            $method = $path[2] . '/' . $this->router->fetch_class() . '/' . $this->router->fetch_method();
            $head_menu = Capsule::select("SELECT COUNT(id) AS COUNT FROM method_access WHERE group_id = '" . $group . "' AND method = '" . $method . "'");
            return ($head_menu[0]['COUNT'] > 0) ? true : false;
        } else {
            if (!$this->input->is_ajax_request())
                show_404();
            else
                return true;
        }
    }

    public function render($folder, $page, $data = null, $menu = true, $js = '', $css = '', $body_class = "", $theme = NULL)
    {
        $template = new Template();
        $this->load->library(array('ion_auth'));

        $admin_groups = array('admin_erp');
        if ($this->ion_auth->in_group($admin_groups)) {
            $template->menuType = 'erp';
        }
        $template->show($folder, $page, $data, $menu, $js, $css, $body_class, $theme);
    }

    public function button_access()
    {

    }
}
	