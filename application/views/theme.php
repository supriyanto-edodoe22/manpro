<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Manajemen Proyek">
  <meta content="Ali Nurdin" name="author">
  <title>MANPRO</title>

  <!--  Default-->
  <link href="<?php echo base_url('assets/images/favico.png'); ?>" rel="icon">
  <link href="<?php echo css_url('bootstrap.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?php echo css_url('dataTables.bootstrap.min.css'); ?>">
  <link href="<?php echo css_url('responsive.bootstrap.min.css'); ?>">
  <link href="<?php echo css_url('bootstrap-multiselect.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?php echo css_url('font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?php echo css_url('toastr.min.css'); ?>" rel="stylesheet" type="text/css">
  <?php echo $css; ?>
  <link href="<?php echo css_url('buttons.dataTables.min.css'); ?>" rel="stylesheet" type="text/css">
  <style>
  /*div.dataTables_processing { z-index: 1; }*/
    .dataTables_info{
      float: left;
    }
    .paging_full_numbers{
      float: right;
    }
    .pagination{
     margin: 0px;
    }
  </style>
</head>
<body class="<?=$body_class?>">
  <?php echo $menu;?>
  <?php echo $content;?>
  <script type="text/javascript">
    var base_url            = '<?=base_url();?>';
    var current_url         = "<?=current_url()?>";
//    var emptyTable          = '<?//=$this->lang->line("emptyTable");?>//';
//    var info                = '<?//=$this->lang->line("info");?>//';
//    var infoEmpty           = '<?//=$this->lang->line("infoEmpty");?>//';
//    var infoFiltered        = '<?//=$this->lang->line("infoFiltered");?>//';
//    var infoPostFix         = '<?//=$this->lang->line("infoPostFix");?>//';
//    var thousands           = '<?//=$this->lang->line("thousands");?>//';
//    var lengthMenu          = '<?//=$this->lang->line("lengthMenu");?>//';
//    var loadingRecords      = '<?//=$this->lang->line("loadingRecords");?>//';
//    var processing          = '<?//=$this->lang->line("processing");?>//';
//    var search              = '<?//=$this->lang->line("search");?>//';
//    var zeroRecords         = '<?//=$this->lang->line("zeroRecords");?>//';
//    var first               = '<?//=$this->lang->line("first");?>//';
//    var last                = '<?//=$this->lang->line("last");?>//';
//    var next                = '<?//=$this->lang->line("next");?>//';
//    var previous            = '<?//=$this->lang->line("previous");?>//';
//    var sortAscending       = '<?//=$this->lang->line("sortAscending");?>//';
//    var sortDescending      = '<?//=$this->lang->line("sortDescending");?>//';
  </script>

  <script type="text/javascript" src="<?php echo js_url('jquery.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo js_url('bootstrap.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo js_url('jquery.dataTables.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo js_url('bootstrap3-typeahead.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo js_url('dataTables.bootstrap.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo js_url('dataTables.responsive.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo js_url('responsive.bootstrap.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo js_url('dataTables.buttons.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo js_url('buttons.flash.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo js_url('jszip.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo js_url('pdfmake.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo js_url('vfs_fonts.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo js_url('buttons.html5.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo js_url('buttons.print.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo js_url('buttons.flash.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo js_url('bootstrap-multiselect.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo js_url('toastr.min.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo js_url('class.js'); ?>"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $('.multiselect-filter').multiselect({
        enableFiltering       : true,
        includeSelectAllOption: true
      });

//      $('.form-grid-filter').on('click', function(e){
//        e.preventDefault();
//      });

      //select all checkboxes
      $(".select_all").change(function(){  //"select all" change
        $("form input:checkbox").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
      });

      //uncheck "select all", if one of the listed checkbox item is unchecked
      $('form input:checkbox').change(function(){ //".checkbox" change
        var count = sum = 0;
        $('form input:checkbox').each(function(i){
          sum++;
          if($(this).is(':checked'))
            count++;
        });

        if((sum-1) == count)
          $(".select_all").prop('checked', true);

        if(false == $(this).prop("checked")){ //if this item is unchecked
          $(".select_all").prop('checked', $(this).prop("checked")); //change "select all" checked status to false
        }
      });
    });

    function ucfirst(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    }

    function printDiv() {
      var divToPrint	= document.getElementById('show_bill');
      var newWin		= window.open('','Print-Window');
      newWin.document.open();
      newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');
      newWin.document.close();
      setTimeout(function(){newWin.close();},10);
    }
  </script>
  <?php echo $js;?>
</body>
</html>