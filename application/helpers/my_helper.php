<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('get_month')){
    function get_month($value){
        switch ($value){
            case 'jan': return "January";
                break;
            case 'feb':	return "February";
                break;
            case 'mar':	return "March";
                break;
            case 'apr':	return "April";
                break;
            case 'may':	return "May";
                break;
            case 'jun':	return "June";
                break;
            case 'jul':	return "July";
                break;
            case 'aug':	return "August";
                break;
            case 'sep':	return "September";
                break;
            case 'oct': return "October";
                break;
            case 'nov': return "November";
                break;
            case 'dec': return "December";
                break;
        }
    }
}

if ( ! function_exists('jumlah_minggu')){
    function jumlah_minggu($tgl_awal, $tgl_akhir){
        $detik = 24 * 3600;
        $tgl_awal = strtotime($tgl_awal);
        $tgl_akhir = strtotime($tgl_akhir);

        $minggu = 0;
        for ($i=$tgl_awal; $i <= $tgl_akhir; $i += $detik)
        {
            if (date('w', $i) == '0'){
                $minggu++;
            }
        }

        return $minggu;
    }
}

if ( ! function_exists('tanggal_minggu')){
    function tanggal_minggu($tgl_awal, $tgl_akhir){
        $detik = 24 * 3600;
        $tgl_awal = strtotime($tgl_awal);
        $tgl_akhir = strtotime($tgl_akhir);

        $minggu = array();
        $count = 0;
        $awal = '';
        for ($i=$tgl_awal; $i <= $tgl_akhir; $i += $detik)
        {
            if (date('w', $i) == '0') {
                $minggu[$count]['akhir'] = date('Y-m-d', $i);
                $count++;
            }elseif ($awal === ''){
                $minggu[$count]['awal'] = date('Y-m-d', $i);
            }elseif ($i===$tgl_akhir){
                $minggu[$count]['akhir'] = date('Y-m-d', $i);
                $count++;
            }
            elseif (date('w', $i) == '1'){
                $minggu[$count]['awal'] = date('Y-m-d', $i);
            }

            $awal = date('w', $i);
        }

        return $minggu;
    }
}

if ( ! function_exists('periode_harian')){
    function periode_harian($tgl_awal, $tgl_akhir){
        $detik = 24 * 3600;
        $tgl_awal = strtotime($tgl_awal);
        $tgl_akhir = strtotime($tgl_akhir);

        $tanggal = array();
        $count = 0;
        for ($i=$tgl_awal; $i <= $tgl_akhir; $i += $detik)
        {
            $tanggal[$count] = date('Y-m-d', $i);
            $count++;
        }

        return $tanggal;
    }
}
