<?php

namespace MyLibraries\Libraries;
use Illuminate\Database\Capsule\Manager as Capsule;

class Template
{

    private $data;
    private $js_file;
    private $css_file;
    private $CI;
    private $menuType;

    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->helper('url');
        $this->menuType = 'hq';
    }

    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

    public function show($folder, $page, $data = null, $menu = true, $js = '', $css = '', $body_class = "", $theme = '')
    {
        if (!file_exists('application/modules/' . $folder . '/views/' . $page . '.php')) {
            show_404();
        } else {
            $this->js_file[] = $js;
            $this->data = $data;
            $this->load_JS($js);
            $this->load_css($css);

            $loadMenu = $this->menuType;

            $this->data['menu'] = ($menu) ? ($this->menuType == 'hq' ? $this->init_menu() : $this->init_menu_erp()) : '';

            $this->data['content'] = $this->CI->load->view($folder . '/' . $page . '.php', $this->data, true);
            $this->data['body_class'] = $body_class;

            $this->CI->load->view(($this->menuType == 'hq' ? 'theme.php' : ($theme != '' ? $theme . '.php' : '')), $this->data);
        }
    }

    private function load_JS($js)
    {
        $this->data['js'] = '';

        if ($js) {
            foreach ($js as $js_file) {
                $this->data['js'] .= "<script type='text/javascript' src=" . js_url($js_file . '.js') . "></script>" . "\n";
            }
        }
    }

    private function load_css($css)
    {
        $this->data['css'] = '';

        if ($css) {
            foreach ($css as $css_file) {
                $this->data['css'] .= "<link rel='stylesheet' type='text/css' href=" . css_url($css_file . '.css') . ">" . "\n";
            }
        }
    }

    private function init_menu()
    {
        $this->CI->load->library('Ion_auth');
        $this->CI->load->model('ion_auth_model');

        $head_menu = Capsule::select("  SELECT * FROM (SELECT
                                            p.order_idx,
                                            m.*
                                        FROM
                                            menu_access p
                                        INNER JOIN menu m ON p.menu_id = m.menu_id
                                        WHERE
                                            p.group_id = '" . $this->CI->ion_auth_model->get_users_groups()->row()->id . "'
                                        AND m.parent_menu IS NULL) AS permission
                                        ORDER BY order_idx ASC;");
        $html = '';
        //strtoupper(strtolower($this->menuType)
        $html .= '   <div class="navbar navbar-default navbar-static-top">
                      <div class="container">
                        <div class="navbar-header">
                          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                          </button>
                          <a class="navbar-brand" href="' . base_url() . '"><span>MANAJEMEN PROYEK </span><br></a>
                        </div>
                        <div class="collapse navbar-collapse" id="navbar-ex-collapse">
                          <ul class="nav navbar-nav navbar-right">
                            <li>
                              <a href="' . base_url('Beranda') . '">Beranda<br></a>
                            </li>';
        foreach ($head_menu as $key => $val) {
            $detail_menu = Capsule::select("SELECT * FROM menu_access INNER JOIN menu ON menu_access.menu_id = menu.menu_id WHERE parent_menu ='" . $val['menu_id'] . "' AND menu_access.group_id = '" . $this->CI->ion_auth_model->get_users_groups()->row()->id . "' ORDER BY order_idx ASC;");
            if (count($detail_menu) > 0) {
                $html .= '  <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">' . $val['name'] . ' <i class="fa fa-caret-down"></i></a>
                              <ul class="dropdown-menu" role="menu">';
                foreach ($detail_menu as $detkey => $detval) {
                    $html .= '  <li>
                                  <a href="' . base_url($detval['url']) . '">' . $detval['name'] . '</a>
                                </li>';
                }
                $html .= '</ul></li>';
            } else
                $html .= '<li class="active">
                            <a href="' . base_url($val['url']) . '">' . $val['name'] . '</a>
                          </li>';
        }

        $html .= '   <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-user"></i> ' . $this->CI->session->userdata('username') . ' <i class="fa fa-caret-down"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li>
                          <a href="' . base_url('auth/logout') . '">Log Out</a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </div>
              </div>
            </div>';

        return $html;
    }

    private function init_menu_erp()
    {
        $this->CI->load->library('Ion_auth');
        $this->CI->load->model('ion_auth_model');
        $head_menu = Capsule::select("  SELECT * FROM (SELECT
                                            p.order_idx,
                                            m.*
                                        FROM
                                            menu_access p
                                        INNER JOIN menu m ON p.menu_id = m.menu_id
                                        WHERE
                                            p.group_id = '" . $this->CI->ion_auth_model->get_users_groups()->row()->id . "'
                                        AND m.parent_menu IS NULL) AS permission
                                        ORDER BY order_idx ASC;");
    }

}
	