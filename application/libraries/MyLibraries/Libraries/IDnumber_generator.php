<?php
namespace MyLibraries\Libraries;
defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Capsule\Manager as Capsule;

class IDnumber_generator
{
    public function __construct()
    {
        $this->CI =& get_instance();
    }

    public function number($num=0,$pre="",$sub="",$long=8)
    {
        return $pre.sprintf("%0".$long."d",$num).$sub;
    }

    public function last_num($table=false, $primary = false,$prefix=false, $subfix=false, $long=9)
    {
        $t      = $this->CI->db->limit(1,0)->order_by($primary, "DESC");
        $length = ($prefix) ? strlen($prefix) : strlen($subfix) ;
        if($prefix)
            $t = $t->where(array('SUBSTR('.$primary.','.$length.','.$length.')' => (string) $prefix));
        if($subfix)
            $t = $t->where(array('SUBSTR('.$primary.','.$long.','.$length.')'=> (string) $subfix));
        $t = $t->get($table);
        $num = $t->num_rows() > 0?$t->row_array()[$primary]:0;

        if($prefix)
            $num = substr($num,strlen($prefix));
        if($subfix)
            $num = substr($num,0-strlen($subfix));

        return ((int)$num) + 1;
    }
}