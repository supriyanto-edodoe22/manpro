var grid_function = function(){
    $('.fa-pencil, .fa-trash').click(function(){ select_data(this); });
}

var select_data = function(element){
    var selected_data =  window.grid_table.table.row( $(element).parents('tr') ).data();
    // console.log(selected_data);

    if($(element).hasClass('fa-pencil')){
        $('[name=id_pekerja]').val(selected_data.id_pekerja);
        $('[name=jenis]').val(selected_data.jenis);
        $('[name=harga]').val(selected_data.harga);
        $('#modal-id').modal('show');
    }else {
        $('#delete-confirm').modal('show');
        $('#delete-confirm .btn-primary').attr('data',selected_data.id_pekerja);
    }
}

var doAction = function(element){
    event.preventDefault();
    var my_ajax  = new ajax,
        action   = $('.primary').val()==''?'insert':'edit';
    fields    = $(element).serializeArray();

    var delete_attribute= typeof $(element).attr('data') !== 'undefined' ? $(element).attr('data') : '';
    var url             = (delete_attribute != '') ? current_url+'/ajax/delete/' : current_url+'/ajax/'+ action;
    var data            = (delete_attribute != '') ? {id: delete_attribute, status: 3} : fields;

    my_ajax.url      = url;
    my_ajax.async    = false;
    my_ajax.data     = data;
    my_ajax.callback = action_callback;
    my_ajax.init();
}

//callback
var action_callback = function(data, status, xhr){
    var dt = JSON.parse(data);
    if(xhr.status == 200){
        if(dt.status){
            toastr.success(dt.message);
            $('#modal-id').modal('hide');
            document.getElementById("form").reset();
        }else{
            toastr.error(dt.message);
        }
    } else
        toastr.error((dt.message==""?"Check your input":dt.message));

    window.grid_table.table.draw();
};

$(document).ready(function() {
     //Datagrid Material
    var opt = {
        id: 'list_pekerja',
        column: [
          { "data": "id_pekerja", "searchable": false,"orderable": false, "visible": false },
          { "data": "no", "searchable": false,"orderable": false },
          { "data": "jenis" },
          { "data": "harga"},
          { "data": "aksi", "orderable": false, "searchable": false }
        ],
        function : grid_function,
        order:[[2,'asc']]
    }

    window.grid_table = new grid(opt);
    window.grid_table.url = current_url + "/grid/list_pekerja";
    window.grid_table.type = 'GET';
    window.grid_table.init();
});