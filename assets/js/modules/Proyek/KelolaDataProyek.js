var grid_function = function () {
    $('.ubah, .redirect_page').click(function () {
        select_data(this);
    });
}

var select_data = function (element) {
    var selected_data = window.grid_table.table.row($(element).parents('tr')).data();

    if ($(element).hasClass('ubah')) {
        $('[name=id_proyek]').val(selected_data.id_proyek);
        $('[name=nama]').val(selected_data.nama);
        $('[name=lokasi]').val(selected_data.lokasi);
        $('[name=pemilik]').val(selected_data.pemilik);
        $('[name=jenis]').val(selected_data.jenis);
        $('[name=tgl_kontrak]').val(selected_data.tgl_kontrak);
        $('[name=nilai]').val(selected_data.nilai);
        $('[name=tgl_mulai]').val(selected_data.tgl_mulai);
        $('[name=tgl_selesai]').val(selected_data.tgl_selesai);
        $('[name=durasi]').val(selected_data.durasi);
        $('[name=deskripsi]').val(selected_data.deskripsi);
        $('[name=catatan]').val(selected_data.catatan);
        $('[name=user_id]').val(selected_data.user_id);
        $('#modal-id').modal('show');
    } else{
        gotopage($(element).attr('page'),selected_data.id_proyek,selected_data.nama);
    }
}

var gotopage = function (url,id,nama) {
    var f = document.getElementById('target', '');
    f.setAttribute('action', base_url + url);
    f.id_proyek.value = id;
    f.nama_proyek.value = nama;
    f.submit();
}

var doAction = function (element) {
    event.preventDefault();
    console.log('tes');
    var my_ajax = new ajax,
        action = $('.primary').val() == '' ? 'insert' : 'edit';
    fields = $(element).serializeArray();

    var delete_attribute = typeof $(element).attr('data') !== 'undefined' ? $(element).attr('data') : '';
    var url = (delete_attribute != '') ? current_url + '/ajax/delete/' : current_url + '/ajax/' + action;
    var data = (delete_attribute != '') ? {id: delete_attribute, status: 3} : fields;

    my_ajax.url = url;
    my_ajax.async = false;
    my_ajax.data = data;
    my_ajax.callback = action_callback;
    my_ajax.init();
}

//callback
var action_callback = function (data, status, xhr) {
    var dt = JSON.parse(data);
    if (xhr.status == 200) {
        if (dt.status) {
            toastr.success(dt.message);
            $('#modal-id').modal('hide');
            document.getElementById("form").reset();
        } else {
            toastr.error(dt.message);
        }
    } else
        toastr.error((dt.message == "" ? "Check your input" : dt.message));

    window.grid_table.table.draw();
};

$(document).ready(function () {
    //Datagrid Pengguna
    var opt = {
        id: 'list_proyek',
        column: [
            {"data": "nama"},
            {"data": "info", "orderable": false, "searchable" : false},
            {"data": "progress", "orderable": false, "searchable" : false},
            {"data": "aksi", "orderable": false, "searchable" : false}
        ],
        function: grid_function,
        order: [[0, 'asc']]
    }
    window.grid_table = new grid(opt);
    window.grid_table.url = current_url + "/grid/list_proyek";
    window.grid_table.type = 'GET';
    window.grid_table.init();

    $('#modal-id').on('hidden.bs.modal', function () { document.getElementById("form").reset(); $('.primary').val('') });
});