var grid_function = function(){
    $('.fa-pencil, .fa-trash').click(function(){ select_data(this); });
}

var select_data = function(element){
    var selected_data =  window.grid_table.table.row( $(element).parents('tr') ).data();
    // console.log(selected_data);

    if($(element).hasClass('fa-pencil')){
        $('[name=kode_resiko]').val(selected_data.kode_resiko);
        $('[name=deskripsi]').val(selected_data.deskripsi);
        $('[name=probabilitas]').val(selected_data.probabilitas);
        $('[name=dampak]').val(selected_data.dampak);
        $('[name=nilai_kepentingan]').val(selected_data.nilai_kepentingan);
        $('[name=tingkat_kepentingan]').val(selected_data.tingkat_kepentingan);
        $('[name=mitigasi]').val(selected_data.mitigasi);
        $('#modal-id').modal('show');
    }else {
        $('#delete-confirm').modal('show');
        $('#delete-confirm .btn-primary').attr('data',selected_data.kode_resiko);
    }
}

var doAction = function(element){
    event.preventDefault();
    var my_ajax  = new ajax,
        action   = $('.primary').val()==''?'insert':'edit';
    fields    = $(element).serializeArray();

    var delete_attribute= typeof $(element).attr('data') !== 'undefined' ? $(element).attr('data') : '';
    var url             = (delete_attribute != '') ? current_url+'/ajax/delete/' : current_url+'/ajax/'+ action;
    var data            = (delete_attribute != '') ? {id: delete_attribute, status: 3} : fields;

    my_ajax.url      = url;
    my_ajax.async    = false;
    my_ajax.data     = data;
    my_ajax.callback = action_callback;
    my_ajax.init();
}

//callback
var action_callback = function(data, status, xhr){
    var dt = JSON.parse(data);
    if(xhr.status == 200){
        if(dt.status){
            toastr.success(dt.message);
            $('#modal-id').modal('hide');
            document.getElementById("form").reset();
        }else{
            toastr.error(dt.message);
        }
    } else
        toastr.error((dt.message==""?"Check your input":dt.message));

    window.grid_table.table.draw();
};

$(document).ready(function() {
     //Datagrid Material
    var opt = {
        id: 'list_resiko',
        column: [

          { "data": "no", "searchable": false,"orderable": false },
          { "data": "kode_resiko"},
          { "data": "deskripsi" },
          { "data": "probabilitas" },
          { "data": "dampak"},
          { "data": "nilai_kepentingan"},
          { "data": "tingkat_kepentingan" },
          { "data": "mitigasi" },
          { "data": "aksi", "orderable": false, "searchable": false }
        ],
        function : grid_function,
        order:[[2,'asc']]
    }

    window.grid_table = new grid(opt);
    window.grid_table.url = current_url + "/grid/list_resiko";
    window.grid_table.type = 'GET';
    window.grid_table.init();

    $('#modal-id').on('hidden.bs.modal', function () { document.getElementById("form").reset(); $('.primary').val('') });
});