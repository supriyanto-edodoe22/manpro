/**
 * Activity Class
 * XE: new Activity({
 *      id: 'A',
 *      duration: 8,
 *          predecessors: ['C','B']
 *  });
 */
function Activity(configs) {
    var self = this;
    configs = configs || {};
    self.id = configs.id;
    self.duration = configs.duration;
    self.est = configs.est; //Earliest Start Time
    self.lst = configs.lst; //Latest Start Time
    self.eet = configs.eet; //Earliest End Time
    self.let = configs.let; //Latest End Time 
    self.h   = configs.h; //clearance (holgura)
    self.successors = [];
    self.predecessors = configs.predecessors || [];
    return self;
}

/**
 * List of Activities Class
 *  
 */
function ActivityList() {
    //Private vars
    var self = this;
    var processed = false;
    var list = {};
    var start, end;

    /**
     * Add an activity to the list
     */
    self.addActivity = function (activity) {
        
        list[activity.id] = activity;
    };

    /**
    * Private method Pre process list
    * Adds and sets the start and end node
    * Replaces successors and predecessors Ids for pointers
    * @throws Error if a predecessor not exists 
    */
    function processList(){
        if(processed){
            return; //Already processed
        }
        processed = true;
        
        start = new Activity({id : 'start', duration : 0});
        
        //Replaces id for pointers to actvities   
        for(var i in list){
            var current = list[i];
            var predecessorsIds = current.predecessors;
            var predecessors = [];

            if(predecessorsIds.length == 0){
                predecessors.push(start);
                start.successors.push(current);
            }else{
                for(var j in predecessorsIds){
                    var previous = list[predecessorsIds[j]];
                    if(!previous){
                        throw new Error('Node ' + j + ' dont exists');
                    }
                    predecessors.push(previous); 
                    previous.successors.push(current); 
                }
            }
            current.predecessors = predecessors;
        }
        
    }

    /**
     * Private function set Earliest Times
     * Earliest Start Time (est) and Earliest End Time (eet)
     * Recursively browse starting from root
     */
    function setEarlyTimes(root){
        for(var i in root.successors){
            var node = root.successors[i];
            
            var predesessors = node.predecessors;
            for (var j in predesessors) {
                var activity = predesessors[j];
                if (node.est == null || node.est < activity.eet)
                    node.est = activity.eet;
            }
            node.eet = node.est + node.duration;
            setEarlyTimes(node);
        }
    }


    /**
     * Private function set Latest Times
     * Latest Start Time (est) and Latest End Time (eet)
     * Recursively browse starting from root
     */
    function setLateTimes(root){
        if(root.successors.length == 0){
            root.let = root.eet;
            root.lst = root.let - root.duration;
            root.h   = root.eet - root.let;
        }else{
            for(var i in root.successors){
                var node = root.successors[i];
                setLateTimes(node);
                if(root.let == null || root.let > node.lst){
                    root.let = node.lst;
                }
            }

            root.lst = root.let - root.duration;
            root.h = root.let - root.eet;
        }
    }


    /**
     * Build Critical Path Tree recursively
     *
     */
    function buildCriticalPath(root, path){
        if(root.h == 0){
            var predecessors = root.predecessors;
            for(var i in predecessors){
                var node = predecessors[i];
                if(node.h == 0){
                    var clone = new Activity({
                        id : node.id,
                        duration : node.duration,
                        est : node.est,
                        lst : node.lst,
                        eet : node.eet,
                        let : node.let,
                        h : node.h
                    });
                    //Dont add start ... its just a fake node
                    if(node !== start){
                        path.predecessors.push(clone);
                        buildCriticalPath(node, clone);
                    }
                    
                }
            }
        }
                        
    }

    /**
     * Applies the PEM, with little modification
     * Returns a Graph subset with the structure of the involved activities 
     */
    self.getCriticalPath = function(endid){
        if(!endid){
            throw new Error('End activity id is required!');
        }

        end = list[endid];

        if(!end){
            throw new Error('Node end dont not match');
        }
        //Make sure that list is well defined
        processList();
        
        //Setup Start Times
        start.est = 0;
        start.eet = 0;
        setEarlyTimes(start);

        //Setup End Times
        end.let = end.eet;
        end.lst = end.let - end.duration;
        end.h   = end.eet - end.let;
        setLateTimes(start);

        //Assemble Critical Path (tree)
        var path = null;
        if(end.h == 0){
            var path = new Activity({
                id : end.id,
                duration : end.duration,
                est : end.est,
                lst : end.lst,
                eet : end.eet,
                let : end.let,
                h   : end.h
            });

            buildCriticalPath(end, path);
        }
        return path;
    };

    /**
     * Get the activity list, does a preprocessing (only once)  
     */
    self.getList = function () {
        processList();
        return list;
    };
    return self;
};

$(document).ready(function () {
    var obj_pekerjaan = JSON.parse(pekerjaan);
    var obj_link_pekerjaan = JSON.parse(link_pekerjaan);

    var cpm = new ActivityList();

    $.get( "ajax/cpm", function( data ) {
        var res = JSON.parse(data);
      // console.log( data );
      
      console.log( "Load was performed." );

      $.each(res.data, function(index, value){
        var pendahulu = JSON.parse(res.data[index].pendahulu);
        var id_kegiatan = res.data[index].id_pekerjaan;

        // console.log('pendahulu',pendahulu)
        // console.log('durasi',res.data[index].durasi)

        if(pendahulu == null)        
            cpm.addActivity(new Activity({
                id: String(id_kegiatan),
                duration: res.data[index].durasi,
            }));
        else
            cpm.addActivity(new Activity({
                id: String(id_kegiatan),
                duration: res.data[index].durasi,
                predecessors: pendahulu
            }));
      });

        var cpm_list = cpm.getList();
        console.log('TABLE CPM', cpm_list);
        var result = cpm.getCriticalPath(String(last_kegiatan));
            console.log(result);

        var critical = [];
          $.each(cpm_list, function(index,value){
            if(value.h == 0)
                critical.push(value.id)
          })

          console.log('kritikal',critical)

        var nodes = null;
        var edges = null;
        var network = null;
        var directionInput = document.getElementById("direction");

        draw();

        function destroy() {
            if (network !== null) {
                network.destroy();
                network = null;
            }
        }

        function draw() {
            destroy();
            nodes = [];
            edges = [];
            var connectionCount = [];
            directionInput.value = "LR";

            $.each(obj_pekerjaan,function(index, value){
                // console.log(value)
                nodes.push({id: value.id_pekerjaan, label: String(value.nama)});
            });

            
            $.each(obj_link_pekerjaan,function(index, value){
                // console.log(value)
                // console.log(critical.indexOf(value.from))
                edges.push({from: value.from, to: value.to, color:{color:(critical.indexOf(value.from) >= 0) ? 'red' : 'black'}, arrows:'to' });
            })

            nodes[0]["level"] = 0;
            nodes[1]["level"] = 1;
            nodes[2]["level"] = 2;
            nodes[3]["level"] = 2;
            nodes[4]["level"] = 4;
            nodes[5]["level"] = 5;


            // create a network
            var container = document.getElementById('network_diagram');
            var data = {
                nodes: nodes,
                edges: edges
            };

            var options = {
                edges: {
                    smooth: {
                        type: 'cubicBezier',
                        forceDirection: (directionInput.value == "UD" || directionInput.value == "DU") ? 'vertical' : 'horizontal',
                        roundness: 0.4
                    }
                },
                layout: {
                    hierarchical: {
                        direction: directionInput.value
                    }
                },
                physics:false
            };
            network = new vis.Network(container, data, options);

            // add event listeners
            network.on('select', function (params) {
                document.getElementById('selection').innerHTML = 'Selection: ' + params.nodes;
            });
        }
    });


    var tasks =  {
        data:JSON.parse(pekerjaan_gantt),
        links:JSON.parse(link_pekerjaan_gantt)
    };

    gantt.config.scale_unit = "day";
    gantt.config.duration_unit = "day";
    gantt.config.date_scale = "%d";


    gantt.config.subscales = [
        {unit:"month", step:1, date:"%F, %Y"}
    ];
    gantt.config.scale_height = 50;

    gantt.config.row_height = 30;
    gantt.config.min_column_width = 30;
    gantt.config.highlight_critical_path = true;

    gantt.init("gantt_here");

    gantt.parse(tasks);


    $('#network_diagram').hide();

    $('.nav li a').click(function(){
        var tab_panel = $(this).attr('href');

        if(tab_panel == '#network_diagram'){
            $('#network_diagram').attr('style','visibility: visible;width: 100%;height: 100%');            
            $('#gantt_here').attr('style','visibility: hidden;display: none');           
        }else{
            $('#network_diagram').attr('style','visibility: hidden;display: none');
            $('#gantt_here').attr('style','visibility: visible;');
        }

        console.log($(this).attr('href'));
    });
});