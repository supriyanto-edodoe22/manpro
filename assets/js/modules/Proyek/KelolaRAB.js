var child_rab_counter = 0;
var grid_function = function(){
    $('.fa-pencil, .fa-trash').click(function(){ select_data(this); });
}

var select_data = function(element){
    var selected_data =  window.grid_table.table.row( $(element).parents('tr') ).data();
    console.log('selected_data',selected_data);

    if($(element).hasClass('fa-pencil')){
        $('.primary').val(selected_data.id_rab);
        $('#id_pekerjaan').val(selected_data.id_pekerjaan);
        $('#id_proyek').val(selected_data.id_proyek);
        $('#nama_pekerjaan').val(selected_data.nama);
        $('#total').val(selected_data.total);

        child_rab_counter= 0;

        $.each(selected_data.detail.material, function(index, value){
            console.log('detail material', value);
            window.child_rab.row.add( [
                '<input type="hidden" name="detail['+child_rab_counter+'][id_detail]" value="'+value.id_rab_material+'" >' +
                '<select name="detail['+child_rab_counter+'][jenis]" class="form-control jenis input-sm">' +
                '<option selected value="rab_material">Material</option>' +
                '<option value="rab_pekerja">Pekerja</option>' +
                '<option value="rab_peralatan">Peralatan</option>' +
                '</select>',
                '<input type="text" class="form-control input-sm auto_rab_row" auto-type="jenis" autocomplete="off" value="'+value.nama+'" required >' +
                '<input type="hidden" class="id_item_child" name="detail['+child_rab_counter+'][id_item_child]" value="'+value.id_material+'">',
                '<input type="text" class="form-control input-sm satuan" name="detail['+child_rab_counter+'][satuan]"  value="'+value.satuan+'" style="width: 100% !important;">',
                '<input type="text" class="form-control input-sm kuantiti" name="detail['+child_rab_counter+'][kuantiti]" onkeyup="kuantiti_calculator(this)" onclick="focusall(this)" value="'+value.kuantitas+'">',
                '<input type="text" class="form-control input-sm harga_satuan" name="detail['+child_rab_counter+'][harga_satuan]" value="'+value.harga+'" readonly>',
                '<input type="text" class="form-control input-sm total" name="detail['+child_rab_counter+'][total]" value="'+(parseFloat(value.kuantitas)*parseFloat(value.harga))+'" readonly>',
                '<i style="cursor: pointer" class="fa fa-trash" aria-hidden="true"></i>'
            ] ).draw( false );
            child_rab_counter++;
        });

        $.each(selected_data.detail.pekerja, function(index, value){
            console.log('detail pekerja', value);
            window.child_rab.row.add( [
                '<input type="hidden" name="detail['+child_rab_counter+'][id_detail]" value="'+value.id_rab_pekerja+'" >' +
                '<select name="detail['+child_rab_counter+'][jenis]" class="form-control jenis input-sm">' +
                '<option value="rab_material">Material</option>' +
                '<option selected value="rab_pekerja">Pekerja</option>' +
                '<option value="rab_peralatan">Peralatan</option>' +
                '</select>',
                '<input type="text" class="form-control input-sm auto_rab_row" auto-type="jenis" autocomplete="off" value="'+value.jenis+'" required >' +
                '<input type="hidden" class="id_item_child" name="detail['+child_rab_counter+'][id_item_child]" value="'+value.id_pekerja+'">',
                '<input type="text" class="form-control input-sm satuan" name="detail['+child_rab_counter+'][satuan]"  value="'+value.satuan+'" style="width: 100% !important;">',
                '<input type="text" class="form-control input-sm kuantiti" name="detail['+child_rab_counter+'][kuantiti]" onkeyup="kuantiti_calculator(this)" onclick="focusall(this)" value="'+value.kuantitas+'">',
                '<input type="text" class="form-control input-sm harga_satuan" name="detail['+child_rab_counter+'][harga_satuan]" value="'+value.harga+'" readonly>',
                '<input type="text" class="form-control input-sm total" name="detail['+child_rab_counter+'][total]" value="'+(parseFloat(value.kuantitas)*parseFloat(value.harga))+'" readonly>',
                '<i style="cursor: pointer" class="fa fa-trash" aria-hidden="true"></i>'
            ] ).draw( false );
            child_rab_counter++;
        });

        $.each(selected_data.detail.peralatan, function(index, value){
            console.log('detail peralatan', value);
            window.child_rab.row.add( [
                '<input type="hidden" name="detail['+child_rab_counter+'][id_detail]" value="'+value.id_rab_peralatan+'" >' +
                '<select name="detail['+child_rab_counter+'][jenis]" class="form-control jenis input-sm">' +
                '<option value="rab_material">Material</option>' +
                '<option value="rab_pekerja">Pekerja</option>' +
                '<option selected value="rab_peralatan">Peralatan</option>' +
                '</select>',
                '<input type="text" class="form-control input-sm auto_rab_row" auto-type="jenis" autocomplete="off" value="'+value.nama+'" required >' +
                '<input type="hidden" class="id_item_child" name="detail['+child_rab_counter+'][id_item_child]" value="'+value.id_peralatan+'">',
                '<input type="text" class="form-control input-sm satuan" name="detail['+child_rab_counter+'][satuan]"  value="'+value.satuan+'" style="width: 100% !important;">',
                '<input type="text" class="form-control input-sm kuantiti" name="detail['+child_rab_counter+'][kuantiti]" onkeyup="kuantiti_calculator(this)" onclick="focusall(this)" value="'+value.kuantitas+'">',
                '<input type="text" class="form-control input-sm harga_satuan" name="detail['+child_rab_counter+'][harga_satuan]" value="'+value.harga+'" readonly>',
                '<input type="text" class="form-control input-sm total" name="detail['+child_rab_counter+'][total]" value="'+(parseFloat(value.kuantitas)*parseFloat(value.harga))+'" readonly>',
                '<i style="cursor: pointer" class="fa fa-trash" aria-hidden="true"></i>'
            ] ).draw( false );
            child_rab_counter++;
        });

        $('#modal-id').modal('show');
    }else {
        $('#delete-confirm').modal('show');
        $('#delete-confirm .btn-primary').attr('data',selected_data.id_rab);
    }
}

var doAction = function(element){
    event.preventDefault();
    var my_ajax  = new ajax,
        action   = $('.primary').val()==''?'insert':'edit';
    fields    = $(element).serializeArray();

    var delete_attribute= typeof $(element).attr('data') !== 'undefined' ? $(element).attr('data') : '';
    var url             = (delete_attribute != '') ? current_url+'/ajax/delete/' : current_url+'/ajax/'+ action;
    var data            = (delete_attribute != '') ? {id: delete_attribute, status: 3} : fields;

    my_ajax.url      = url;
    my_ajax.async    = false;
    my_ajax.data     = data;
    my_ajax.callback = action_callback;
    my_ajax.init();
}

//callback
var action_callback = function(data, status, xhr){
    var dt = JSON.parse(data);
    if(xhr.status == 200){
        if(dt.status){
            toastr.success(dt.message);
            $('#modal-id').modal('hide');
            document.getElementById("form").reset();
        }else{
            toastr.error(dt.message);
        }
    } else
        toastr.error((dt.message==""?"Check your input":dt.message));

    window.grid_table.table.draw();
};

var footer = function(api, row, data, start, end, display){
    // var api = window.grid_table, data;
    console.log(api);

    // Remove the formatting to get integer data for summation
    var intVal = function ( i ) {
        return typeof i === 'string' ?
            i.replace(/[\$,]/g, '')*1 :
            typeof i === 'number' ?
                i : 0;
    };

    // Total over all pages
    total = api
        .column( 4 )
        .data()
        .reduce( function (a, b) {
            return intVal(a) + intVal(b);
        }, 0 );

    // Update footer
    var pajak = total * (10/100);

    $( api.column( 4 ).footer() ).html(
        pajak
    );

    $(".total").text(pajak + total);
}

$(document).ready(function() {
     //Datagrid Material
    var opt = {
        id: 'list_rab',
        column: [

          { "data": null, "class" : "details-control", "searchable": false, "orderable": false, "defaultContent": "" },
          { "data": "no"},
          { "data": "nama" },
          // { "data": "satuan" },
          { "data": "volume"},
          // { "data": "harga_satuan"},
          { "data": "total" },
          { "data": "aksi", "orderable": false, "searchable": false }
        ],
        function : grid_function,
        footerFunction : footer,
        order:[[2,'asc']]
    }

    window.grid_table = new grid(opt);
    window.grid_table.url = current_url + "/grid/list_rab";
    window.grid_table.type = 'GET';
    window.grid_table.init();

    $('#modal-id').on('hidden.bs.modal', function () { 
        document.getElementById("form").reset();
        window.child_rab.clear().draw();
        $('.primary').val('');
    });

    child_rab_counter = 0;
    window.child_rab = $('#child_rab').DataTable({
        dom: 'Bfrtip',
        "bFilter": false,
        buttons: [
            {
                text: 'Tambah',
                action: function ( e, dt, node, config ) {
                    dt.row.add( [
                        '<select name="detail['+child_rab_counter+'][jenis]" class="form-control jenis input-sm">' +
                        '<option value="rab_material">Material</option>' +
                        '<option value="rab_pekerja">Pekerja</option>' +
                        '<option value="rab_peralatan">Peralatan</option>' +
                        '</select>',
                        '<input type="text" class="form-control input-sm auto_rab_row" auto-type="jenis" autocomplete="off" required ><input type="hidden" class="id_item_child" name="detail['+child_rab_counter+'][id_item_child]">',
                        '<input type="text" class="form-control input-sm satuan" name="detail['+child_rab_counter+'][satuan]" style="width: 100% !important;">',
                        '<input type="text" class="form-control input-sm kuantiti" name="detail['+child_rab_counter+'][kuantiti]" onkeyup="kuantiti_calculator(this)" onclick="focusall(this)">',
                        '<input type="text" class="form-control input-sm harga_satuan" name="detail['+child_rab_counter+'][harga_satuan]" readonly>',
                        '<input type="text" class="form-control input-sm total" name="detail['+child_rab_counter+'][total]" readonly>',
                        '<i style="cursor: pointer" class="fa fa-trash" aria-hidden="true"></i>'
                    ] ).draw( false );
                    console.log(child_rab_counter);
             
                    child_rab_counter++;
                }
            }
        ], "fnDrawCallback": function(oSettings) {
            var that = this;
            var $input = $('.auto_rab_row');
        $('.auto_rab_row').on('keyup', function(){
            var t = this;
            var data = $(t).closest('tr').find('.jenis').val(); 
            $(t).typeahead({
                source: function(query, process){
                    console.log(data);

                    switch(data) {
                        case 'rab_material':
                            return process(material);
                            break;
                        case 'rab_peralatan':
                            return process(peralatan);
                            break;
                        case 'rab_pekerja':
                            return process(pekerja);
                            break;
                    }

                    return process(data_pekerjaan);
                }, 
                autoSelect: true,
                afterSelect: function(item){
                    console.log(item);
                    // $('#id_pekerjaan').val(item.id);
                    var sum =0;
                    
                    $(t).closest('tr').find('.id_item_child').val(item.id);
                    $(t).closest('tr').find('.kuantiti').val(1);
                    $(t).closest('tr').find('.harga_satuan').val(item.harga);
                    $(t).closest('tr').find('.total').val(item.harga);

                    $('.total').each(function(){
                        sum += parseFloat(this.value);
                    });

                    $('#total').val(sum);
                    console.log(sum);
                }
            });

        });

            $(".jenis").on('change', function(){
                $(this).closest('tr').find('.auto_rab_row').val('');
            });

            $('#child_rab .fa-trash').on("click",function(){
                // that.table.row($(this).parents('tr')).remove().draw( false );
                window.child_rab.row( $(this).parents('tr') ).remove().draw(false);
            });
        }
    });

    var $input = $('.auto_rab');
    $input.typeahead({
        source: function(query, process){
            var data = $input.attr('auto-type');

            switch(data) {
                case 'pekerjaan':
                    return process(data_pekerjaan);
                    break;
            }

            console.log(data);

        }, 
        autoSelect: true,
        afterSelect: function(item){
            console.log(item);
            $('#id_pekerjaan').val(item.id);
        }
    });


    // Array to track the ids of the details displayed rows
    var detailRows = [];

    $('#list_rab tbody').on( 'click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = window.grid_table.table.row( tr );
        var idx = $.inArray( tr.attr('id'), detailRows );
 
        if ( row.child.isShown() ) {
            tr.removeClass( 'details' );
            row.child.hide();
 
            // Remove from the 'open' array
            detailRows.splice( idx, 1 );
        }
        else {
            tr.addClass( 'details' );
            row.child( format( row.data() ) ).show();
 
            // Add to the 'open' array
            if ( idx === -1 ) {
                detailRows.push( tr.attr('id') );
            }
        }
    } );

    window.grid_table.table.on( 'draw', function () {
        $.each( detailRows, function ( i, id ) {
            $('#'+id+' td.details-control').trigger( 'click' );
        } );
    } );

});

function format ( d ) {
    var html = '<table class="table">' +
            '<caption>Material</caption>' +
            '<thead>' +
            '<tr><td>No</td><td>Nama</td><td>Satuan</td><td>Kuantitas</td><td>Harga</td><td>Total</td></tr>' +
            '</thead><tbody>';
            
    $.each(d.detail.material, function(index, value){
        console.log('material',value)
        html += '<tr><td>'+(index+1)+'</td>' +
                '<td>'+value.nama+'</td>' +
                '<td>'+value.satuan+'</td>' + 
                '<td>'+value.kuantitas+'</td>' + 
                '<td>'+parseFloat(value.harga)+'</td>' + 
                '<td>'+(parseFloat(value.kuantitas) * parseFloat(value.harga))+'</td><tr>';
    });

    html +=  '</tbody>';

    html += '<table class="table">' +
            '<caption>Peralatan</caption>' +
            '<thead>' +
            '<tr><td>No</td><td>Nama</td><td>Satuan</td><td>Kuantitas</td><td>Harga</td><td>Total</td></tr>' +
            '</thead><tbody>';

    $.each(d.detail.peralatan, function(index, value){
        console.log('peralatan',value)
        html += '<tr><td>'+(index+1)+'</td>' +
                '<td>'+value.nama+'</td>' +
                '<td>'+value.satuan+'</td>' + 
                '<td>'+value.kuantitas+'</td>' + 
                '<td>'+parseFloat(value.harga)+'</td>' + 
                '<td>'+(parseFloat(value.kuantitas) * parseFloat(value.harga))+'</td><tr>';
    });

    html +=  '</tbody>';

    html += '<table class="table">' +
            '<caption>Pekerja</caption>' +
            '<thead>' +
            '<tr><td>No</td><td>Nama</td><td>Satuan</td><td>Kuantitas</td><td>Harga</td><td>Total</td></tr>' +
            '</thead><tbody>';

    $.each(d.detail.pekerja, function(index, value){
        console.log('pekerja',value)
        html += '<tr><td>'+(index+1)+'</td>' +
                '<td>'+value.jenis+'</td>' +
                '<td>'+value.satuan+'</td>' + 
                '<td>'+value.kuantitas+'</td>' + 
                '<td>'+parseFloat(value.harga)+'</td>' + 
                '<td>'+(parseFloat(value.kuantitas) * parseFloat(value.harga))+'</td><tr>';
    });

    html +=  '</tbody>';

    return html;
}

var kuantiti_calculator = function(element){
    var total = $(element).val() * $(element).closest('tr').find('.harga_satuan').val();
    var sum = 0;
    $(element).closest('tr').find('.total').val(total);
    focusall(element);

    $('.total').each(function(){
        sum += parseFloat(this.value);
    });

    $("#total").val(sum);

    console.log(sum);
}

var focusall = function(element){
    $(element).select();
}