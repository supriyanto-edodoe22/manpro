var grid_function = function(){
    $('.fa-pencil, .fa-trash').click(function(){ select_data(this); });
}

var select_data = function(element){
    var selected_data =  window.grid_table.table.row( $(element).parents('tr') ).data();
    // console.log(selected_data);

    if($(element).hasClass('fa-pencil')){
        var data_pendahulu = JSON.parse(selected_data.pendahulu);
        var text_selected = '';
        console.log(data_pendahulu)
        $("#pendahulu option").each(function() {
            var check = (data_pendahulu != null) ? data_pendahulu.indexOf($(this).val()) : -1;
            $(this).attr('selected', false);
            $(this).attr('disabled', false);

            if (check >= 0) {
                $(this).attr('selected', true)
                text_selected += $(this).text() + ',';
            }

            if($(this).val() == selected_data.id_pekerjaan)
                $(this).attr('disabled', true)

            console.log('val', $(this).val())
            console.log('cek', check)
        });

        $('.multiselect-native-select .btn-group input').each(function() {
            var check = (data_pendahulu != null) ? data_pendahulu.indexOf($(this).val()) : -1;
            $(this).prop('checked', false)
            $(this).attr('disabled', false)
            console.log($(this).val());

            if (check >= 0) 
                $(this).prop('checked', true)

            if($(this).val() == selected_data.id_pekerjaan)
                $(this).attr('disabled', true)

            console.log(check)
        });

        $('.multiselect-selected-text').text((text_selected === '') ? 'None selected' : text_selected);

        $('[name=id_pekerjaan]').val(selected_data.id_pekerjaan);
        // $('[name=pendahulu]').val(selected_data.pendahulu);
        $('[name=nama]').val(selected_data.nama);
        $('[name=durasi]').val(selected_data.durasi);
        $('[name=bobot]').val(selected_data.bobot);
        $('[name=volume]').val(selected_data.volume);
        $('[name=mulai]').val(selected_data.mulai);
        $('[name=selesai]').val(selected_data.selesai);
        $('#modal-id').modal('show');
    }else {
        $('#delete-confirm').modal('show');
        $('#delete-confirm .btn-primary').attr('data',selected_data.id_pekerjaan);
    }
}

var doAction = function(element){
    event.preventDefault();
    var my_ajax  = new ajax,
        action   = $('.primary').val()==''?'insert':'edit';
    fields    = $(element).serializeArray();

    var delete_attribute= typeof $(element).attr('data') !== 'undefined' ? $(element).attr('data') : '';
    var url             = (delete_attribute != '') ? current_url+'/ajax/delete/' : current_url+'/ajax/'+ action;
    var data            = (delete_attribute != '') ? {id: delete_attribute, status: 3} : fields;

    my_ajax.url      = url;
    my_ajax.async    = false;
    my_ajax.data     = data;
    my_ajax.callback = action_callback;
    my_ajax.init();
}

//callback
var action_callback = function(data, status, xhr){
    var dt = JSON.parse(data);
    if(xhr.status == 200){
        if(dt.status){
            toastr.success(dt.message);
            $('#modal-id').modal('hide');
            document.getElementById("form").reset();
        }else{
            toastr.error(dt.message);
        }
    } else
        toastr.error((dt.message==""?"Check your input":dt.message));

    window.grid_table.table.draw();
};

var cal_durasi = function(){
    if($('#mulai').val() != '' && $('#selesai').val() != '')
        $('#durasi').val(daydiff(parseDate($('#mulai').val()), parseDate($('#selesai').val())));
}

var parseDate = function(str) {
    var mdy = str.split('-');
    return new Date(mdy[0], mdy[1]-1, mdy[2]);
}

var daydiff = function(first, second) {
    return Math.round((second-first)/(1000*60*60*24));
}

$(document).ready(function() {
     //Datagrid Struktur Kerja
    var opt = {
        id: 'list_struktur_kerja',
        column: [
          { "data": "id_pekerjaan", "searchable": false,"orderable": false, "visible": false },
          { "data": "no", "searchable": false,"orderable": false },
          { "data": "nama"},
          { "data": "durasi"},
          { "data": "bobot"},
          { "data": "volume"},
          { "data": "mulai"},
          { "data": "selesai"},
          { "data": "aksi", "orderable": false, "searchable": false }
        ],
        function : grid_function,
        order:[[2,'asc']]
    }

    window.grid_table = new grid(opt);
    window.grid_table.url = current_url + "/grid/list_struktur_kerja";
    window.grid_table.type = 'GET';
    window.grid_table.init();

    // $("#pendahulu").select2();
    $('#pendahulu').multiselect({
        enableFiltering: true,
        includeSelectAllOption: true
    });

    $('#modal-id').on('hidden.bs.modal', function () {
        document.getElementById("form").reset(); 
        $('.primary').val('');
        $('.multiselect-selected-text').text('None selected');
        $("#pendahulu option").each(function() {
            $(this).attr('selected', false);
            $(this).attr('disabled', false);
        });

        $('.multiselect-native-select .btn-group input').each(function() {
            $(this).prop('checked', false)
            $(this).attr('disabled', false)
        });
    });
});