var grid_function = function(){
    $('.fa-pencil, .fa-trash').click(function(){ select_data(this); });
}

var select_data = function(element){
    var selected_data =  window.grid_table.table.row( $(element).parents('tr') ).data();
    // console.log(selected_data);

    if($(element).hasClass('fa-pencil')){
        $('[name=id_material]').val(selected_data.id_material);
        $('[name=nama]').val(selected_data.nama);
        $('[name=satuan]').val(selected_data.satuan);
        $('[name=harga]').val(selected_data.harga);
        $('#modal-id').modal('show');
    }else {
        $('#delete-confirm').modal('show');
        $('#delete-confirm .btn-primary').attr('data',selected_data.id_material);
    }
}

var doAction = function(element){
    event.preventDefault();
    var my_ajax  = new ajax,
        action   = $('.primary').val()==''?'insert':'edit';
    fields    = $(element).serializeArray();

    var delete_attribute= typeof $(element).attr('data') !== 'undefined' ? $(element).attr('data') : '';
    var url             = (delete_attribute != '') ? current_url+'/ajax/delete/' : $(element).attr('action');
    var data            = (delete_attribute != '') ? {id: delete_attribute, status: 3} : fields;

    my_ajax.url      = url;
    my_ajax.async    = false;
    my_ajax.data     = data;
    my_ajax.callback = action_callback;
    my_ajax.init();
}

//callback
var action_callback = function(data, status, xhr){
    var dt = JSON.parse(data);
    if(xhr.status == 200){
        if(dt.status){
            toastr.success(dt.message);
            $('#modal-id').modal('hide');
            document.getElementById("form").reset();
            location.reload();
        }else{
            toastr.error(dt.message);
        }
    } else
        toastr.error((dt.message==""?"Check your input":dt.message));

    // window.grid_table.table.draw();
};

$(document).ready(function() {
    var jsonData = {
        "data": [{
            "One": "Row 1 - Field 1",
            "Two": "Row 1 - Field 2",
            "Three": "Row 1 - Field 3"
        }, {
            "One": "Row 2 - Field 1",
            "Two": "Row 2 - Field 2",
            "Three": "Row 2 - Field 3"
        }],
        "columns": [{
            "title": "One",
            "data": "One"
        }, {

            "title": "Two",
            "data": "Two"
        }, {
            "title": "Three",
            "data": "Three"
        }]
    }

    //Datagrid Progress
    // var opt = {
    //     id: 'list',
    //     column: [
    //         { "data": "id_realisasi", "searchable": false,"orderable": false, "visible": false },
    //         { "data": "nama" },
    //         { "data": "bobot", "searchable": false,"orderable": false},
    //         { "data": "bobot_realisasi", "searchable": false,"orderable": false},
    //         { "data": "volume", "searchable": false,"orderable": false},
    //         { "data": "volume_realisasi", "searchable": false,"orderable": false},
    //         // { "data": "aksi", "orderable": false, "searchable": false }
    //     ],
    //     function : grid_function,
    //     order:[[2,'asc']],
    //     scrollX: true
    // }
    //
    // window.grid_table = new grid(opt);
    // window.grid_table.url = base_url + "Proyek/InputProgress/grid/list_progress_mingguan";
    // window.grid_table.type = 'GET';
    // window.grid_table.server = false;
    // window.grid_table.init();
    $('#list').dataTable({
        "bLengthChange": false,
        "bFilter": false,
        "ordering": false,
        "scrollX": true
        // "scrollX": true
    });

    $("#id_pekerjaan").select2();
    $('#modal-id').on('hidden.bs.modal', function () { document.getElementById("form").reset(); $('.primary').val('') });
});