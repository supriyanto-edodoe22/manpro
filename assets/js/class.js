// AJAX FUNCTION
var ajax = function(){
  return {
    url           : false,
    method        : "POST",
    data          : {},
    async         : true,
    callback      : false,
    error_callback: false,
    cache         : false,
    init          : function(){
      var t = this;
      if(t.url){
         $.ajax({
           url    : t.url,
           method : t.method,
           async  : t.async,
           cache  : t.cache,
           data   : t.data,
           success: function(d,s,x){
             if(t.callback){
               if(typeof t.callback === 'function')
                 t.callback(d,s,x);
             } else
               t.callback = d;
           },
           error  : function(x,s){
             if(t.error_callback){
               if(typeof t.error_callback === 'function')
               t.error_callback(x,s);
               else
               console.log(x);
             } else
              console.log(x);
           }
         });
      }
    }
  }
}

String.prototype.ucfirst = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

var serialize = function(obj, prefix) {
  var str = [];
  for(var p in obj) {
    if (obj.hasOwnProperty(p)) {
      var k = prefix ? prefix + "[" + p + "]" : p, v = obj[p];
      str.push(typeof v == "object" ?
        serialize(v, k) :
        encodeURIComponent(k) + "=" + encodeURIComponent(v));
    }
  }
  return str.join("&");
}

var upCase = function(t){
  if(t !== null)
    return t.charAt(0).toUpperCase() + t.slice(1);
}

var count = function(t){
  var c = 0;
  for( var k in t){
    if(t.hasOwnProperty(k))
      c++;
  }
  return c;
}

// DATAGRID WITH DETAIL
function grid(r) {
  var v     = typeof r == 'object'?r:false;
  var grid  = {
    id      : v?(typeof v.id !== 'undefined'?v.id:'grid'):'grid',
    server  : v?(typeof v.server !== 'undefined'? v.server:true):true,
    search  : v?(typeof v.search !== 'undefined'? v.search:true):true,
    url     : v?(typeof v.url === 'function'?v.url:false):false,
    type    : v?(typeof v.type === 'function'?v.type:'POST'):'POST',
    detail  : v?(typeof v.detail === 'function'?v.detail:false):false,
    column  : v?(typeof v.column === 'object'?v.column:false):false,
    columnDefs  : v?(typeof v.column !== 'object'?v.column:false):false,
    data    : v?(typeof v.data === 'object'?v.data:{}):{},
    pagination  : v?(typeof v.pagination === 'object'?v.pagination:true):true,
    scrollX  : v?(typeof v.scrollX === 'object'?v.scrollX:false):false,
    info  : v?(typeof v.info !== 'undefined'?v.info:true):true,
    length  : v?(typeof v.length !== 'undefined'?v.length:false):false,
    filter  : v?(typeof v.filter === 'object'?v.filter:false):false,
    ordering   : v?(typeof v.ordering !== 'undefined'?v.ordering:true):true,
    order   : v?(typeof v.order === 'object'?v.order:false):false,
    button  : v?(typeof v.button === 'object'?v.button:[]):[],
    function: v?(typeof v.function === 'function'?v.function:false):false,
    table   : false,
    footerFunction: v?(typeof v.footerFunction === 'function'?v.footerFunction:false):false,
    init    : function(){
      var t = this;
      if(t.url){
        t.table = $('#'+t.id).DataTable({
          "serverSide"  : t.server,
          "processing"  : true,
          "responsive"  : true,
          "stateSave"   : true,
          "columns"     : t.column,
          "pagingType"  : "full_numbers",
          "dom"         : 'Bfrtip',
          "buttons"     : t.button,
          "searching"   : t.search,
          "ajax"        : {
            'url'   : t.url,
            'type'  : t.type,
            'data'  : function ( d ) {
              var count = 0;
              for (var k in t.data) {
                if (t.data.hasOwnProperty(k)) {
                  ++count;
                }
              }

              if(count > 0)
                d = t.data;

              return d;
            },
            'async' : false
          },
          "deferRender": true,
          "fnServerParams" : function(ds){
            var tmp = {};
            if(t.filter){
              $('.'+t.filter).each(function(k,v){
                if($(this).attr('name') !== 'undefined' && $(this).val() !== '')
                  tmp[$(this).attr('name')] = $(this).val();
              });
            }
            ds.filter = tmp;
          },
          "info"          : t.info,
          "ordering"      : t.ordering,
          "order"         : t.order?t.order:[[1,'asc']],
          "paging"        : t.pagination,
          "scrollX"       : t.scrollX,
          "bLengthChange" : t.length,
          "fnDrawCallback": function(settings){
            if(t.detail){
              var detailRows = [];
              $('#'+t.id+' tbody tr td.details-control').click(function(){
                var tr  = $(this).closest('tr');
                var row = t.table.row( tr );
                var idx = $.inArray( tr.attr('id'), detailRows );
                if ( row.child.isShown() ) {
                  tr.removeClass( 'details' );
                  row.child.hide();

                  // Remove from the 'open' array
                  detailRows.splice( idx, 1 );
                } else {
                  tr.addClass( 'details' );
                  row.child( t.detail(row.data(),$(this)) ).show();
                  // Add to the 'open' array
                  if ( idx === -1 ) {
                    detailRows.push( tr.attr('id') );
                  }
                }
              });
              $.each( detailRows, function ( i, id ) {
                $('#'+id+' td.details-control').trigger( 'click' );
              } );
            }

            if(t.function){
              t.function();
            }
          },
          "footerCallback": function ( row, data, start, end, display ) {
						if(typeof row !== 'undefined'){
							if(t.footerFunction){
                                							var api   = this.api(), data;
								t.footerFunction(api, row, data, start, end, display);
							}
                          /*FOOTER*/

//							var info  = api.page.info();
//
//							 // Remove the formatting to get integer data for summation
//							 var intVal = function ( i ) {
//							 return typeof i === 'string' ?
//							 i.replace(/[\$,]/g, '')*1 :
//							 typeof i === 'number' ?
//							 i : 0;
//							 };
//
//							 if(info.page+1 === info.pages){
//							 total = api
//							 .column( 5 )
//							 .data()
//							 .reduce( function (a, b) {
//							 return intVal(a) + intVal(b);
//							 }, 0 );
//
//							 var sum = api.data();
//
//							 $('.datagrid-footer').attr('style','');
//							 $( api.column( 0 ).footer() ).html('Total Members');
//							 $( api.column( 1 ).footer() ).html(sum.length);
//							 $( api.column( 4 ).footer() ).html('Total Credits');
//							 $( api.column( 5 ).footer() ).html(total);
//							 }else{
//							 $('.datagrid-footer').attr('style', 'display:none');
//							 }
						}
          }
        });
      }
    },
    getTablesID:function()
    {
      return this.table;
    }
  };
  return grid;
}