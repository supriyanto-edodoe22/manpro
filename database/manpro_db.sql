CREATE DATABASE  IF NOT EXISTS `manpro _db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `manpro _db`;
-- MySQL dump 10.13  Distrib 5.7.16, for Linux (x86_64)
--
-- Host: localhost    Database: manpro _db
-- ------------------------------------------------------
-- Server version	5.7.16-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'admin','Administrator'),(2,'Mandor','Mandor'),(3,'Pelaksana Teknis','Pelaksana Teknis'),(4,'Penanggung Jawab','Penanggung Jawab Teknis'),(5,'Direktur','Direktur');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `material`
--

DROP TABLE IF EXISTS `material`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `material` (
  `id_material` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `satuan` varchar(255) NOT NULL,
  `harga` double NOT NULL,
  PRIMARY KEY (`id_material`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `material`
--

LOCK TABLES `material` WRITE;
/*!40000 ALTER TABLE `material` DISABLE KEYS */;
INSERT INTO `material` VALUES (1,'Besi (Baja) Tulangan','Kg (Kg)',1000);
/*!40000 ALTER TABLE `material` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `url` text,
  `parent_menu` int(11) DEFAULT NULL,
  `icon_class` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,'Proyek','-','Proyek/KelolaDataProyek',NULL,''),(2,'Kelola data proyek','-','Proyek/KelolaDataProyek',99,''),(3,'Kelola rencana kerja','-','Proyek/KelolaRencanaKerja',99,NULL),(4,'Kelola jadwal pekerjaan','-','Proyek/KelolaJadwalPekerjaan',99,NULL),(6,'Manajemen Resiko\r\n','-','Proyek/ManajemenResiko',99,NULL),(7,'Pengendalian Proyek','-','Proyek/PengendalianProyek',99,NULL),(8,'Kelola RAB','-','Proyek/KelolaRAB',99,NULL),(9,'Input Progress','-','Proyek/InputProgress',99,NULL),(10,'Konfigurasi','-','#',NULL,NULL),(11,'Kelola harga material','-','Konfigurasi/KelolaHargaMaterial',10,NULL),(12,'Kelola harga peralatan','-','Konfigurasi/KelolaHargaPeralatan',10,NULL),(13,'Kelola harga pekerja','-','Konfigurasi/KelolaHargaPekerja',10,NULL),(14,'Kelola struktur kerja','-','Proyek/KelolaStrukturKerja',99,NULL),(15,'Pengguna','-','Konfigurasi/KelolaPengguna',10,NULL),(16,'Laporan','-','#',NULL,NULL),(17,'Data Proyek','-','Laporan/DataProyek',16,NULL),(18,'Proyek Periode','-','Laporan/ProyekPeriode',16,NULL),(19,'Rencana Kerja','-','Laporan/RencanaKerja',16,NULL),(20,'RAB\r\n',NULL,'Laporan/RAB',16,NULL),(22,'Manajemen Resiko','-','Laporan/ManajemenResiko',16,NULL),(23,'Jadwal Proyek','-','Laporan/JadwalProyek',16,NULL);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_access`
--

DROP TABLE IF EXISTS `menu_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `group_id` mediumint(8) DEFAULT NULL,
  `order_idx` int(11) NOT NULL,
  `action` text,
  PRIMARY KEY (`id`),
  KEY `fk_menu_access_groups_1` (`group_id`),
  KEY `fk_menu_access_groups_2` (`menu_id`),
  CONSTRAINT `fk_menu_access_groups_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`),
  CONSTRAINT `fk_menu_access_groups_2` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_access`
--

LOCK TABLES `menu_access` WRITE;
/*!40000 ALTER TABLE `menu_access` DISABLE KEYS */;
INSERT INTO `menu_access` VALUES (1,1,1,0,NULL),(2,2,1,1,NULL),(3,3,1,2,NULL),(4,4,1,3,NULL),(6,6,1,5,NULL),(7,7,1,6,NULL),(8,8,1,7,NULL),(9,9,1,8,NULL),(10,10,1,0,NULL),(11,11,1,1,NULL),(12,12,1,2,NULL),(13,13,1,3,NULL),(14,14,1,4,NULL),(15,15,1,5,NULL),(16,16,1,0,NULL),(17,17,1,1,NULL),(18,18,1,2,NULL),(19,19,1,3,NULL),(20,20,1,4,NULL),(22,22,1,6,NULL),(23,23,1,7,NULL);
/*!40000 ALTER TABLE `menu_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `method_access`
--

DROP TABLE IF EXISTS `method_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `method_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `method` text NOT NULL COMMENT 'Class/Method',
  `group_id` mediumint(8) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `fk_method_access_1` (`group_id`),
  CONSTRAINT `fk_method_access_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `method_access`
--

LOCK TABLES `method_access` WRITE;
/*!40000 ALTER TABLE `method_access` DISABLE KEYS */;
INSERT INTO `method_access` VALUES (1,'Beranda/Beranda/index',1,'Beranda'),(2,'Proyek/KelolaDataProyek/index',1,'Kelola Data Proyek'),(3,'Konfigurasi/KelolaPengguna/index',1,'Kelola data pengguna'),(4,'Konfigurasi/KelolaHargaMaterial/index',1,'Kelola Harga Material'),(5,'Konfigurasi/KelolaHargaPeralatan/index',1,'Kelola Harga Peralatan'),(6,'Konfigurasi/KelolaHargaPekerja/index',1,'Kelola Harga Pekerja'),(7,'Proyek/ManajemenSDM/index',1,'Manajemen SDM'),(8,'Proyek/ManajemenResiko/index',1,'Manajemen Resiko'),(9,'Proyek/KelolaStrukturKerja/index',1,'Kelola Struktur Kerja'),(10,'Proyek/InputProgress/harian',1,'Input Progress Harian'),(11,'Proyek/InputProgress/mingguan',1,'Input Progress Mingguan'),(13,'Proyek/InputProgress/index',1,NULL),(14,'Proyek/KelolaRAB/index',1,NULL),(15,'Proyek/KelolaRencanaKerja/index',1,NULL),(16,'Proyek/KelolaJadwalPekerjaan/index',1,NULL);
/*!40000 ALTER TABLE `method_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pekerja`
--

DROP TABLE IF EXISTS `pekerja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pekerja` (
  `id_pekerja` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(50) NOT NULL,
  `harga` double NOT NULL,
  PRIMARY KEY (`id_pekerja`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pekerja`
--

LOCK TABLES `pekerja` WRITE;
/*!40000 ALTER TABLE `pekerja` DISABLE KEYS */;
INSERT INTO `pekerja` VALUES (1,'PEKERJA 1',200);
/*!40000 ALTER TABLE `pekerja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pekerjaan`
--

DROP TABLE IF EXISTS `pekerjaan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pekerjaan` (
  `id_pekerjaan` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `durasi` int(11) NOT NULL,
  `bobot` double NOT NULL,
  `volume` double NOT NULL,
  `pendahulu` text,
  `mulai` date NOT NULL,
  `selesai` date NOT NULL,
  `id_proyek` varchar(19) DEFAULT NULL,
  PRIMARY KEY (`id_pekerjaan`),
  KEY `fk_pekerjaan_proyek_1` (`id_proyek`),
  CONSTRAINT `fk_pekerjaan_1` FOREIGN KEY (`id_proyek`) REFERENCES `proyek` (`id_proyek`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pekerjaan`
--

LOCK TABLES `pekerjaan` WRITE;
/*!40000 ALTER TABLE `pekerjaan` DISABLE KEYS */;
INSERT INTO `pekerjaan` VALUES (100000001,'A',10,3,4,NULL,'2017-01-01','2017-01-11','100000001'),(100000002,'B',5,2,2,'[\"100000001\"]','2017-01-12','2017-01-17','100000001'),(100000003,'C',25,2,2,'[\"100000001\",\"100000002\"]','2017-01-16','2017-02-10','100000001'),(100000004,'D',10,6,6,'[\"100000002\"]','2017-02-10','2017-02-20','100000001'),(100000005,'E',5,6,6,'[\"100000002\",\"100000003\",\"100000004\"]','2017-02-20','2017-02-25','100000001'),(100000006,'F',3,1,1,'[\"100000005\"]','2017-02-25','2017-02-28','100000001');
/*!40000 ALTER TABLE `pekerjaan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `peralatan`
--

DROP TABLE IF EXISTS `peralatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `peralatan` (
  `id_peralatan` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `harga` double NOT NULL,
  PRIMARY KEY (`id_peralatan`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `peralatan`
--

LOCK TABLES `peralatan` WRITE;
/*!40000 ALTER TABLE `peralatan` DISABLE KEYS */;
INSERT INTO `peralatan` VALUES (1,'PERALATAN 1',500);
/*!40000 ALTER TABLE `peralatan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proyek`
--

DROP TABLE IF EXISTS `proyek`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proyek` (
  `id_proyek` varchar(19) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `jenis` varchar(50) NOT NULL,
  `pemilik` varchar(25) NOT NULL,
  `nilai` int(11) NOT NULL,
  `tgl_kontrak` date NOT NULL,
  `tgl_mulai` date NOT NULL,
  `tgl_selesai` date NOT NULL,
  `durasi` int(11) NOT NULL,
  `lokasi` varchar(50) NOT NULL,
  `deskripsi` varchar(150) DEFAULT NULL,
  `catatan` varchar(150) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `status` varchar(30) NOT NULL,
  PRIMARY KEY (`id_proyek`),
  KEY `fk_user_proyek` (`user_id`),
  CONSTRAINT `fk_proyek_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proyek`
--

LOCK TABLES `proyek` WRITE;
/*!40000 ALTER TABLE `proyek` DISABLE KEYS */;
INSERT INTO `proyek` VALUES ('100000001','TES','Aktifitas Harian','TES',11999,'2016-12-25','2016-12-27','2017-02-28',20,'TES','TES','TES',13,'');
/*!40000 ALTER TABLE `proyek` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rab`
--

DROP TABLE IF EXISTS `rab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rab` (
  `id_rab` int(11) NOT NULL AUTO_INCREMENT,
  `id_proyek` varchar(19) NOT NULL,
  `total` int(11) NOT NULL DEFAULT '0',
  `id_pekerjaan` int(11) NOT NULL,
  PRIMARY KEY (`id_rab`),
  KEY `fk_rab_proyek_1` (`id_proyek`),
  CONSTRAINT `fk_proyek_rab` FOREIGN KEY (`id_proyek`) REFERENCES `proyek` (`id_proyek`)
) ENGINE=InnoDB AUTO_INCREMENT=100000008 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rab`
--

LOCK TABLES `rab` WRITE;
/*!40000 ALTER TABLE `rab` DISABLE KEYS */;
INSERT INTO `rab` VALUES (100000006,'100000001',6400,100000002),(100000007,'100000001',9100,100000002);
/*!40000 ALTER TABLE `rab` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rab_material`
--

DROP TABLE IF EXISTS `rab_material`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rab_material` (
  `id_rab_material` int(11) NOT NULL AUTO_INCREMENT,
  `id_material` int(11) NOT NULL,
  `satuan` varchar(255) DEFAULT NULL,
  `kuantitas` double DEFAULT '0',
  `id_rab` int(11) NOT NULL,
  PRIMARY KEY (`id_rab_material`),
  KEY `fk_rab` (`id_rab`),
  KEY `fk_rab_material` (`id_material`),
  CONSTRAINT `fk_rab` FOREIGN KEY (`id_rab`) REFERENCES `rab` (`id_rab`),
  CONSTRAINT `fk_rab_material` FOREIGN KEY (`id_material`) REFERENCES `material` (`id_material`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rab_material`
--

LOCK TABLES `rab_material` WRITE;
/*!40000 ALTER TABLE `rab_material` DISABLE KEYS */;
INSERT INTO `rab_material` VALUES (6,1,'',5,100000006),(7,1,'Kg (Kg)',6,100000007);
/*!40000 ALTER TABLE `rab_material` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rab_pekerja`
--

DROP TABLE IF EXISTS `rab_pekerja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rab_pekerja` (
  `id_rab_pekerja` int(11) NOT NULL AUTO_INCREMENT,
  `id_pekerja` int(11) NOT NULL,
  `satuan` varchar(255) DEFAULT NULL,
  `kuantitas` double DEFAULT '0',
  `id_rab` int(11) NOT NULL,
  PRIMARY KEY (`id_rab_pekerja`),
  KEY `fk_sub_pekerja_rab` (`id_rab`),
  KEY `fk_rab_pekerja` (`id_pekerja`),
  CONSTRAINT `fk_rab_pekerja` FOREIGN KEY (`id_pekerja`) REFERENCES `pekerja` (`id_pekerja`),
  CONSTRAINT `fk_sub_pekerja_rab` FOREIGN KEY (`id_rab`) REFERENCES `rab` (`id_rab`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rab_pekerja`
--

LOCK TABLES `rab_pekerja` WRITE;
/*!40000 ALTER TABLE `rab_pekerja` DISABLE KEYS */;
INSERT INTO `rab_pekerja` VALUES (3,1,'',7,100000006),(4,1,'-',8,100000007);
/*!40000 ALTER TABLE `rab_pekerja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rab_peralatan`
--

DROP TABLE IF EXISTS `rab_peralatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rab_peralatan` (
  `id_rab_peralatan` int(11) NOT NULL AUTO_INCREMENT,
  `id_peralatan` int(11) NOT NULL,
  `satuan` varchar(255) DEFAULT NULL,
  `kuantitas` double DEFAULT '0',
  `id_rab` int(11) NOT NULL,
  PRIMARY KEY (`id_rab_peralatan`),
  KEY `fk_sub_rab_peralatan` (`id_rab`),
  KEY `fk_rab_peralatan` (`id_peralatan`),
  CONSTRAINT `fk_rab_peralatan` FOREIGN KEY (`id_peralatan`) REFERENCES `peralatan` (`id_peralatan`),
  CONSTRAINT `fk_sub_rab_peralatan` FOREIGN KEY (`id_rab`) REFERENCES `rab` (`id_rab`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rab_peralatan`
--

LOCK TABLES `rab_peralatan` WRITE;
/*!40000 ALTER TABLE `rab_peralatan` DISABLE KEYS */;
INSERT INTO `rab_peralatan` VALUES (2,1,'bh',3,100000007);
/*!40000 ALTER TABLE `rab_peralatan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `realisasi_pekerjaan`
--

DROP TABLE IF EXISTS `realisasi_pekerjaan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `realisasi_pekerjaan` (
  `id_realisasi` int(11) NOT NULL AUTO_INCREMENT,
  `id_pekerjaan` int(11) NOT NULL,
  `volume_realisasi` double DEFAULT NULL,
  `bobot_realisasi` double DEFAULT NULL,
  `satuan` varchar(255) NOT NULL,
  `dari` date NOT NULL,
  `sampai` date NOT NULL,
  `tipe` tinyint(1) NOT NULL COMMENT '1 : Harian, 2 : Mingguan',
  PRIMARY KEY (`id_realisasi`),
  KEY `fk_realisasi_pekerjaan` (`id_pekerjaan`),
  CONSTRAINT `fk_realisasi_pekerjaan` FOREIGN KEY (`id_pekerjaan`) REFERENCES `pekerjaan` (`id_pekerjaan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `realisasi_pekerjaan`
--

LOCK TABLES `realisasi_pekerjaan` WRITE;
/*!40000 ALTER TABLE `realisasi_pekerjaan` DISABLE KEYS */;
/*!40000 ALTER TABLE `realisasi_pekerjaan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rencana_pekerjaan`
--

DROP TABLE IF EXISTS `rencana_pekerjaan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rencana_pekerjaan` (
  `id_rencana` int(11) NOT NULL AUTO_INCREMENT,
  `id_pekerjaan` int(11) NOT NULL,
  `bobot_rencana` double NOT NULL,
  `dari` date NOT NULL,
  `sampai` date NOT NULL,
  `tipe` tinyint(1) NOT NULL COMMENT '1 : Harian, 2 : Mingguan',
  PRIMARY KEY (`id_rencana`),
  KEY `fk_realisasi_pekerjaan` (`id_pekerjaan`),
  CONSTRAINT `rencana_pekerjaan_ibfk_1` FOREIGN KEY (`id_pekerjaan`) REFERENCES `pekerjaan` (`id_pekerjaan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rencana_pekerjaan`
--

LOCK TABLES `rencana_pekerjaan` WRITE;
/*!40000 ALTER TABLE `rencana_pekerjaan` DISABLE KEYS */;
/*!40000 ALTER TABLE `rencana_pekerjaan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resiko`
--

DROP TABLE IF EXISTS `resiko`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resiko` (
  `kode_resiko` int(11) NOT NULL AUTO_INCREMENT,
  `deskripsi` varchar(255) NOT NULL,
  `probabilitas` int(11) NOT NULL,
  `dampak` int(11) NOT NULL,
  `nilai_kepentingan` int(11) NOT NULL,
  `tingkat_kepentingan` varchar(15) NOT NULL,
  `mitigasi` varchar(150) NOT NULL,
  `id_proyek` varchar(19) NOT NULL,
  PRIMARY KEY (`kode_resiko`),
  KEY `fk_resiko_proyek_1` (`id_proyek`),
  CONSTRAINT `fk_resiko_proyek` FOREIGN KEY (`id_proyek`) REFERENCES `proyek` (`id_proyek`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resiko`
--

LOCK TABLES `resiko` WRITE;
/*!40000 ALTER TABLE `resiko` DISABLE KEYS */;
/*!40000 ALTER TABLE `resiko` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET latin1 NOT NULL,
  `password` text CHARACTER SET latin1 NOT NULL,
  `nama` varchar(50) CHARACTER SET latin1 NOT NULL,
  `no_telp` varchar(14) CHARACTER SET latin1 NOT NULL,
  `email` varchar(25) CHARACTER SET latin1 NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `remember_code` varchar(255) DEFAULT NULL,
  `created_on` varchar(255) DEFAULT NULL,
  `last_login` varchar(255) DEFAULT NULL,
  `active` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'administrator','$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36','Admin','','admin@admin.com',NULL,'gRqeINJq0nQSHnTXZuw7TO','1268889823','1485719942','1',''),(12,'Root','$2y$08$ec1bBowhNm3FfPUo9C3iruopkRsnBnLu/0qTSqZKp93q21d2qh.Iy','Edo Supriyanto','','koston_murder@yahoo.com',NULL,NULL,'1480137824','1480146260','1','85795420428'),(13,'test','$2y$08$pRrZpEqC.uQIvPNVqnrfguEGQqPbozXR3h.FTBwl8bH1Edx9r.7SK','tset','','koston_murder@yahoo.coms',NULL,NULL,'1480148058',NULL,'1',''),(14,'TES2','$2y$08$J4YbXBLOrgw7sqcfIYWjnOQ78OmQEIrHbFViztOl8ZZ1Z422iKz36','TEST 2','','edodoe.murder@gmail.com',NULL,NULL,'1481116818',NULL,'1','081320248581');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` mediumint(8) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `fk_users_groups_groups_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`),
  CONSTRAINT `fk_users_groups_groups_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_groups`
--

LOCK TABLES `users_groups` WRITE;
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;
INSERT INTO `users_groups` VALUES (1,1,1),(10,12,1),(16,13,2),(19,14,2);
/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-30  7:19:51
